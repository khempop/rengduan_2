import UIKit
import Flutter
import GoogleMaps
import Firebase

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    FirebaseConfiguration.shared.setLoggerLevel(.min)
    FirebaseApp.configure() //add this before the code below
    GMSServices.provideAPIKey("AIzaSyBFRWFU6NVJg9QaD10rDdfADo9DvpdBn50")
    GeneratedPluginRegistrant.register(with: self)

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
