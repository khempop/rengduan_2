import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/launch_screen.dart';
import 'package:rengduan/login_screen.dart';
import 'package:rengduan/screen/customer/choose_tectnicain/ChooseTechnicain_screen.dart';
import 'package:rengduan/screen/customer/complete_work/UserCompleteWrok_screen.dart';
import 'package:rengduan/screen/customer/confirm_request/ConfirmRequest_screen.dart';
import 'package:rengduan/screen/customer/into/into_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/select_work_type/select_worktype_screen.dart';
import 'package:rengduan/screen/customer/tracking_technicain/TrackingTechnicain_screen.dart';
import 'package:rengduan/screen/customer/verify/user_verify_screen.dart';
import 'package:rengduan/screen/customer/waiiting_technicain/WaitingTechnicain_screen.dart';
import 'package:rengduan/screen/select_user_type_screen.dart';
import 'package:cron/cron.dart';
import 'package:rengduan/screen/technician/complete/TechnicianComplete_screen.dart';
import 'package:rengduan/screen/technician/into/TechnicianIntoApp_screen.dart';
import 'package:rengduan/screen/technician/register/TechnicianRegister_screen.dart';
import 'package:rengduan/screen/technician/tracking/TechnicianTracking_screen.dart';
import 'package:rengduan/screen/technician/verify/TechnicainVerify_screen.dart';
import 'package:rengduan/screen/technician/worklist/TechnicianWorkList_screen.dart';
import 'package:rengduan/screen/technician/deposit_point/deposit_point.dart';

Future<void> main() async {
  final cron = Cron();
  cron.schedule(Schedule.parse('*/5 * * * * *'), () async {
    //print('every three minutes');
    getLocation();
  });
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

String CurrentUserId = "";
String GlobalLat = "";
String GlobalLong = "";
String GlobalToken = "";
String GlobalWorkType = "";
String GlobalInURL = "";
String paymentURL = "https://www.sopasss.com";
List<Bank> banks_ = [];
List<Topup> topups_ = [];
String globalsPaymentStatus = "";
String globalsPaymentId = "";
String globalsUserEmail = "";

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool singedin = true;
  late FirebaseMessaging messaging;
  @override
  void initState() {
    super.initState();
    // signOut();
    checkLogin();
    registerNotification();
    Bank.getBankList();
    Topup.getTopupList();
  }

  signOut() {
    GoogleSignIn().signOut();
    FirebaseAuth.instance.signOut();
    SharedPreferencesHelper.setUserID("");
    SharedPreferencesHelper.setUserEmail("");
    SharedPreferencesHelper.setUserTel("");
  }

  Widget _page = LaunchScreen();
  String _workId = "";
  checkLogin() async {
    print("check Login");
    String uid = await SharedPreferencesHelper.getUserID();
    CurrentUserId = uid;
    if (uid != "") {
      var api = ApiProvider();

      List<WorkOrderModel> currentWorkList = await api.getCurrnetWorking(uid);

      bool workInprocess = false;
      currentWorkList.forEach((element) {
        if (element.servicestatus == "accept") {
          workInprocess = true;
          return;
        }
      });

      var userinfo = await api.fetchUserInfoById(uid);
      UserModel.id = userinfo.uid;
      UserModel.profileName = userinfo.name + " " + userinfo.surname;
      UserModel.email = userinfo.email;
      UserModel.imageUrl = userinfo.profileImage;
      globalsUserEmail = userinfo.email;
      if (workInprocess) {
        currentWorkList.forEach((element) {
          _workId = element.uid;
          if (element.servicestatus == "accept") {
            setState(() {
              _page = TrackingTechnician(workid: _workId);
            });
            return;
          }
        });
      } else {
        api.fetchUserInfoById(uid).then((userinfo) {
          GlobalWorkType = userinfo.servicetype;

          if (userinfo.usertype == "user") {
            setState(() {
              _page = SelectWorkType();
            });
          } else {
            if (userinfo.userstatus == "inactive") {
              setState(() {
                _page = TechnicianVerify();
              });
            } else {
              setState(() {
                _page = TechnicianWorkList();
              });
            }
          }
        }).onError((error, stackTrace) => toLogIn());
      }
    } else {
      setState(() {
        _page = Login();
      });
    }
  }

  toLogIn() {
    setState(() {
      _page = Login();
    });
  }

  setImageProfile() {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        UserModel.imageUrl = user.photoURL.toString();
      }
    });
  }

  Future<void> registerNotification() async {
    messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }

    messaging.getToken().then((value) {
      GlobalToken = value.toString();
      print("token= $value");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        fontFamily: "NotoSans",
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      home: _page,
      //home: Login(),
    );
  }
}

Future<void> getLocation() async {
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return;
    }
  }

  location.getLocation().then((_locationData) {
    //print("_locationData.latitude");
    // print(_locationData.latitude);
    GlobalLat = _locationData.latitude.toString();
    GlobalLong = _locationData.longitude.toString();

    if (CurrentUserId != "") {
      addUserLocation(CurrentUserId, _locationData.latitude.toString(),
              _locationData.longitude.toString())
          .then((value) => null);
    }
  });
}

Future<void> addUserLocation(String userid, String lat, String long) {
  CollectionReference users =
      FirebaseFirestore.instance.collection('users_location');
  //print("update location");
  return users
      .doc(userid)
      .set({
        'uid': userid,
        'lat': lat,
        'long': long,
        'servicetype': GlobalWorkType,
        'token': GlobalToken
      })
      .then((value) {})
      .catchError((error) => print("Failed to add user: $error"));
}

class Bank {
  String bankName = "";
  String code = "";

  Bank(this.bankName, this.code);

  void main() {
    getBankList();
  }

  static getBankList() async {
    Client client = Client();
    String endpoint = "https://www.sopasss.com";
    final response = await client.get(Uri.parse(endpoint + "/banklist/"),
        headers: {"Content-Type": "application/json"});
    List<Bank> k = [];
    if (response.statusCode == 200) {
      var m = json.decode(response.body);
      for (var item in m.toList()) {
        k.add(Bank(item["bank_name"], item["code"]));
      }
      banks_ = k;
    }
  }
}

class Topup {
  String price = "";
  String point = "";

  Topup(this.price, this.point);

  void main() {
    getTopupList();
  }

  static getTopupList() async {
    Client client = Client();
    String endpoint = "https://www.sopasss.com";
    final response = await client.get(Uri.parse(endpoint + "/pointlist/"),
        headers: {"Content-Type": "application/json"});
    List<Topup> T = [];
    if (response.statusCode == 200) {
      var m = json.decode(response.body);
      for (var item in m.toList()) {
        T.add(Topup(item["price"], item["point"]));
      }
      topups_ = T;
    }
  }
}
