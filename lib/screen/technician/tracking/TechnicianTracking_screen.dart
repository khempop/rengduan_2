import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rengduan/Constant.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/cancel/TechnicianCancelWork_screen.dart';
import 'package:rengduan/screen/technician/complete/TechnicianComplete_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class TechnicianTracking extends StatefulWidget {
  final String workid;
  const TechnicianTracking({Key? key, required this.workid}) : super(key: key);

  @override
  _TechnicianTrackingState createState() => _TechnicianTrackingState();
}

class _TechnicianTrackingState extends State<TechnicianTracking> {
  BitmapDescriptor? _markerIcon;
  BitmapDescriptor? _markerIcon2;
  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),
    zoom: 14.4746,
  );
  LatLng _provicePosition = LatLng(0, 0);
  LatLng _userPosition = LatLng(0, 0);
  List<Marker> _markers = <Marker>[];
  bool mapLoad = false;

  String _userAddress = "";
  String _providerAddress = "";

  String _userName = "";
  String _userImage = "";
  var api = ApiProvider();

  @override
  void initState() {
    api.getWorkingStatus(widget.workid).then((value) {
      getUserInf(value.userid);
      setState(() {
        var lat = double.parse(value.userlat);
        var long = double.parse(value.userlong);
        _userPosition = LatLng(lat, long);
        _provicePosition =
            LatLng(double.parse(GlobalLat), double.parse(GlobalLong));
        _kGooglePlex = CameraPosition(
          target: LatLng(lat, long),
          zoom: 13.4746,
        );
        mapLoad = true;
        getDurationAddress(_provicePosition, _userPosition);
      });
    });
    super.initState();
  }

  void getUserInf(String uid) {
    api.fetchUserInfoById(uid).then((value) {
      setState(() {
        _userName = value.name + " " + value.surname;
        _userImage =
            value.profileImage != "" ? value.profileImage : AvatarImage;
      });
    });
  }

  getDurationAddress(LatLng provider, LatLng user) {
    print("direction name");
    api
        .getPlacesFromGoogle(
            user.latitude.toString(), user.longitude.toString())
        .then((value) {
      setState(() {
        _userAddress = value.addressName;
      });
    });

    api
        .getPlacesFromGoogle(
            provider.latitude.toString(), provider.longitude.toString())
        .then((value) {
      setState(() {
        _providerAddress = value.addressName;
      });
    });
  }

  Marker _createMarker() {
    if (_markerIcon != null) {
      return Marker(
          markerId: MarkerId("marker_1"),
          position: _provicePosition,
          icon: _markerIcon!,
          onTap: () {
            openMap(_userPosition.latitude, _userPosition.longitude);
          });
    } else {
      return Marker(
          markerId: MarkerId("marker_1"),
          position: _provicePosition,
          onTap: () {
            openMap(_userPosition.latitude, _userPosition.longitude);
          });
    }
  }

  Marker _createMarker2() {
    if (_markerIcon != null) {
      return Marker(
          markerId: MarkerId("marker_2"),
          position: _userPosition,
          icon: _markerIcon2!,
          onTap: () {
            openMap(_provicePosition.latitude, _provicePosition.longitude);
          });
    } else {
      return Marker(
          markerId: MarkerId("marker_2"),
          position: _userPosition,
          onTap: () {
            openMap(_provicePosition.latitude, _provicePosition.longitude);
          });
    }
  }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size.square(20));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/images/marker_start.png')
          .then(_updateBitmap);
    }
  }

  Future<void> _createMarkerImageFromAsset2(BuildContext context) async {
    if (_markerIcon2 == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size.square(20));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/images/marker_des.png')
          .then(_updateBitmap2);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    print("_updateBitmap1");
    setState(() {
      _markerIcon = bitmap;
    });
  }

  void _updateBitmap2(BitmapDescriptor bitmap) {
    print("_updateBitmap2");
    setState(() {
      _markerIcon2 = bitmap;
    });
  }

  Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  @override
  Widget build(BuildContext context) {
    _createMarkerImageFromAsset(context);
    _createMarkerImageFromAsset2(context);
    return new Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                '',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - 300,
            child: mapLoad
                ? GoogleMap(
                    // markers: Set<Marker>.of(_markers),
                    markers: <Marker>{_createMarker(), _createMarker2()},
                    mapType: MapType.normal,
                    initialCameraPosition: _kGooglePlex,
                    myLocationButtonEnabled: false,
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  )
                : Center(child: CircularProgressIndicator()),
          ),
          Align(
            alignment: FractionalOffset.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Container(
                height: 450,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: Colors.white,
                ),
                child: Container(
                  height: 450,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Card(
                            child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              CircleAvatar(
                                radius: 50,
                                backgroundImage: NetworkImage(_userImage),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15, top: 8),
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: Text(
                                          "ผู้รับบริการ",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 18,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15),
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: Text(_userName,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 18,
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )),
                        Container(
                          child: ListTile(
                            /*leading: Image(
                              height: 50,
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/worktype2.png"),
                            ),*/
                            leading: Icon(
                              Icons.person,
                              color: HexColor.fromHex('#F36E21'),
                            ),
                            title: Text(_providerAddress,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                  fontSize: 15,
                                )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 25),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: SizedBox(
                              height: 30,
                              width: 4,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                  color: HexColor.fromHex('#F36E21'),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: ListTile(
                            /*leading: Image(
                              height: 50,
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/worktype2.png"),
                            ),*/
                            leading: Icon(
                              Icons.pin_drop,
                              color: HexColor.fromHex('#F36E21'),
                            ),
                            title: Text(_userAddress,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                  fontSize: 15,
                                )),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 20, left: 10, right: 10),
                          height: 56.0,
                          decoration: BoxDecoration(
                            color: HexColor.fromHex('#1E62AD'),
                            borderRadius: BorderRadius.circular(28),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey,
                                blurRadius: 2,
                                offset: Offset(1, 1), // Shadow position
                              ),
                            ],
                          ),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => TechnicianComplete(
                                            workId: widget.workid,
                                          )));
                            },
                            child: Text(
                              "สิ้นสุดการให้บริการ",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                          height: 56.0,
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.red),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(28),
                          ),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => TechnicianCancelWork(
                                            workId: widget.workid,
                                          )));
                            },
                            child: Text(
                              "ยกเลิก",
                              style: TextStyle(color: Colors.red, fontSize: 16),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
