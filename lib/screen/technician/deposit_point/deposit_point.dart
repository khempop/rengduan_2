import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/login_screen.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/cart/cart.dart';
import 'package:rengduan/screen/technician/internet_banking/internet_banking.dart';
import 'package:rengduan/screen/technician/verify/TechnicainVerify_screen.dart';
import 'package:intl/intl.dart';

import '../../../Constant.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart' show Client;

const p_key = "pkey_test_5jpu9ltamqamgscm6js";
const s_key = "skey_test_5jpu9ltakc0o6qbgncf";
const publicKey = p_key;
String? group1Value;
String? group2Value;

class DepositPoint extends StatefulWidget {
  DepositPoint({Key? key}) : super(key: key);

  @override
  _DepositPointState createState() => new _DepositPointState();
}

class _DepositPointState extends State<DepositPoint> {
  Cart cart = new Cart();

  String endpoint = "https://www.sopasss.com";

  List topuplistList = [1, 2];

  @override
  void initState() {
    super.initState();
  }

  setSelectedBank(Bank bank) {
    setState(() {
      group1Value = bank.code;
    });
  }

  setSelectedTopup(Topup topup) {
    setState(() {
      group2Value = topup.price;
    });
  }

  @override
  Widget build(BuildContext context) {
    var objects = [];

    return Container(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Scaffold(
          body: Stack(children: [
            AppBar(
              iconTheme: IconThemeData(
                color: Colors.black, //change your color here
              ),
              elevation: 0,
              backgroundColor: Colors.transparent,
              title: Column(
                children: [
                  Container(
                    child: Text(
                      'เติม Point ในการใช้งาน',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: InkWell(
                    onTap: () {},
                    child: CircleAvatar(
                        backgroundImage: NetworkImage(UserModel.imageUrl)),
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 120, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "เลือกธนาคารสำหรับแลก Point",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.80,
                          child: Column(
                            children: createRadioListBanks(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "เลือกจำนวนเงิน",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.80,
                          child: Column(
                            children: createRadioListTopup(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: MediaQuery.of(context).size.width * 0.90,
                  margin: EdgeInsets.all(20),
                  height: 56.0,
                  decoration: BoxDecoration(
                    color: Color(0xFF00B9BD),
                    borderRadius: BorderRadius.circular(28),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2,
                        offset: Offset(1, 1), // Shadow position
                      ),
                    ],
                  ),
                  child: TextButton(
                    onPressed: () async {
                      await paymentCreateSource(
                          group1Value!, int.parse(group2Value!));
                      //}
                    },
                    child: Text(
                      "ตกลง",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          ]),
        ),
      ),
    );
  }

  // GestureDetector(
  //   onTap: () async {
  //     await paymentCreateSource(
  //         _method.toString().split('.').last, 3000);
  //   },
  //   child: Container(
  //     padding: EdgeInsets.fromLTRB(50, 10, 50, 10),
  //     decoration: BoxDecoration(
  //       color: Color(0xFF00B9BD),
  //       border: Border.all(
  //           width: 1.0, color: Color(0xFFD7D7D7)),
  //       borderRadius: BorderRadius.circular(15.0),
  //       boxShadow: [
  //         BoxShadow(
  //           color: Colors.grey.withOpacity(0.3),
  //           spreadRadius: 3,
  //           blurRadius: 5,
  //           offset: Offset(
  //               0, 0), // changes position of shadow
  //         ),
  //       ],
  //     ),
  //     child: Column(
  //       children: <Widget>[
  //         Text(
  //           "ตกลง",
  //           style: TextStyle(
  //               color: Color(0xFFFFFFFF),
  //               fontSize: 18,
  //               height: 1),
  //         ),
  //         Text(
  //           "OK",
  //           style: TextStyle(
  //             color: Color(0xFFE4E4E4),
  //             fontSize: 14,
  //           ),
  //         ),
  //       ],
  //     ),
  //   ),
  // ),
  List<Widget> createRadioListBanks() {
    setState(() {
      Bank.getBankList();
    });
    List<Widget> widgets = [];
    for (Bank bank in banks_) {
      widgets.add(
        RadioListTile(
          title: Text(bank.bankName),
          value: bank.code,
          groupValue: group1Value,
          selected: group1Value == bank.code,
          activeColor: Colors.blueAccent,
          onChanged: (value) {
            setState(() {
              group1Value = value as String?;
              print(group1Value);
            });
          },
        ),
      );
    }
    return widgets;
  }

  List<Widget> createRadioListTopup() {
    setState(() {
      Topup.getTopupList();
    });
    List<Widget> widgets = [];
    for (Topup topup in topups_) {
      widgets.add(
        RadioListTile(
          title: Text(topup.point +
              " (" +
              (int.parse(topup.price) / 100).toString() +
              " Baht)"),
          value: topup.price,
          groupValue: group2Value,
          selected: group2Value == topup.point,
          activeColor: Colors.blueAccent,
          onChanged: (value) {
            setState(() {
              group2Value = value as String?;
              print(group2Value);
            });
          },
        ),
      );
    }
    return widgets;
  }

  paymentCreateSource(String bank, int amount) async {
    // ignore: unused_local_variable
    String thisPaymentId = "";
    var url = paymentURL + '/charges/';
    var body = json.encode({
      'amount': amount,
      'source_type': bank,
      'email': globalsUserEmail,
    });

    var uris = Uri.parse(url);
    await http
        .post(
      uris,
      headers: {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Content-Type': 'application/json',
      },
      body: body,
    )
        .then((value) {
      print(value.body);
      print("khem");
      GlobalInURL = json.decode(value.body)["authorize_uri"];
      checkPaymentStatus(json.decode(value.body)["charge_status"],
          json.decode(value.body)["cid"]);

      globalsPaymentStatus = json.decode(value.body)["charge_status"];
      globalsPaymentId = json.decode(value.body)["cid"];
      thisPaymentId = json.decode(value.body)["cid"];
      //print(json.decode(value.body)["cid"]);
      if (globalsPaymentStatus == "pending") {
        MaterialPageRoute materialPageRoute =
            MaterialPageRoute(builder: (BuildContext buildContext) {
          return InternetBanking();
        });
        Navigator.of(context)
            .push(materialPageRoute)
            .then((value) => setState(() {
                  Navigator.of(context).pop();
                }));
        //print("refresh done ");
      }
    });
  }

  checkPaymentStatus(paymentStatus, paymentId) async {
    //driver position -==========================
    // if (paymentStatus == "successful") {
    //   final databaseReference = FirebaseFirestore.instance;
    //   databaseReference
    //       .collection("service")
    //       .where("caseid", isEqualTo: globals.serviceNumberId)
    //       .get()
    //       .then((querySnapshot) {
    //     querySnapshot.docs.forEach((doc) async {
    //       databaseReference.collection("service").doc(doc.get("id")).update({
    //         "payment_status": paymentStatus,
    //         "payment_charge_id": paymentId
    //       });
    //     });
    //   });
    // }
  }

  // paymentCreateCharge(String bank, int amount) async {
  //   final capability = await omise.capability.retrieve();
  // }
}
