import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:rengduan/main.dart';

class InternetBanking extends StatefulWidget {
  @override
  InternetBankingState createState() => InternetBankingState();
}

class InternetBankingState extends State<InternetBanking> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ชำระเงิน'),
      ),
      body: WebView(
        initialUrl: GlobalInURL,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
