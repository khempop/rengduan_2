import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/cancel/TechnicianCancelWork_screen.dart';
import 'package:rengduan/screen/technician/tracking/TechnicianTracking_screen.dart';
import 'package:rengduan/screen/technician/worklist/TechnicianWorkList_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:video_player/video_player.dart';

class WorkDetail extends StatefulWidget {
  final String caseId;
  const WorkDetail({Key? key, required this.caseId}) : super(key: key);

  @override
  _WorkDetailState createState() => _WorkDetailState();
}

class _WorkDetailState extends State<WorkDetail> {
  BitmapDescriptor? _markerIcon;

  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),
    zoom: 14.4746,
  );
  LatLng _userPosition = LatLng(0, 0);
  String _eventStatus = "";
  bool mapDisplay = false;
  String _note = "";

  bool previewVideo = false;
  bool previewImage = false;
  var _imagePreview = "";
  var _videoPreview = "";
  late VideoPlayerController _videoController;
  var progressShow = false;
  var api = ApiProvider();
  @override
  void initState() {
    api.getWorkingStatus(widget.caseId).then((value) {
      setState(() {
        print(CurrentUserId);
        _kGooglePlex = CameraPosition(
          target:
              LatLng(double.parse(value.userlat), double.parse(value.userlong)),
          zoom: 14.4746,
        );
        _userPosition =
            LatLng(double.parse(value.userlat), double.parse(value.userlong));
        _note = value.message;
        _eventStatus = value.servicestatus;
        if (value.commentimageurl != "") {
          //แสดงรูป
          previewVideo = false;
          _imagePreview = value.commentimageurl;
          previewImage = true;
        } else if (value.commentvdourl != "") {
          //แสดงวิดีโอ
          previewVideo = true;
          _videoPreview = value.commentvdourl;
          previewImage = false;
          _videoController = VideoPlayerController.network(_videoPreview)
            ..initialize().then((_) {
              setState(() {
                progressShow = false;
              });
            });
        }
        mapDisplay = true;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _videoController.dispose();

    super.dispose();
  }

  Marker _createMarker() {
    if (_markerIcon != null) {
      return Marker(
          markerId: MarkerId("marker_1"),
          position: _userPosition,
          icon: _markerIcon!,
          onTap: () {
            openMap(_userPosition.latitude, _userPosition.longitude);
          });
    } else {
      return Marker(
          markerId: MarkerId("marker_1"),
          position: _userPosition,
          onTap: () {
            openMap(_userPosition.latitude, _userPosition.longitude);
          });
    }
  }

  Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size.square(20));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/images/marker_des.png')
          .then(_updateBitmap);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    print("_updateBitmap1");
    setState(() {
      _markerIcon = bitmap;
    });
  }

  @override
  Widget build(BuildContext context) {
    _createMarkerImageFromAsset(context);
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Column(
            children: [
              Container(
                child: Text(
                  'รายละเอียดการขอรับบริการ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Container(
                  height: 300,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: mapDisplay
                        ? GoogleMap(
                            // markers: Set<Marker>.of(_markers),
                            markers: <Marker>{_createMarker()},
                            mapType: MapType.normal,
                            initialCameraPosition: _kGooglePlex,
                            myLocationButtonEnabled: false,
                            onMapCreated: (GoogleMapController controller) {
                              _controller.complete(controller);
                            },
                          )
                        : Center(child: CircularProgressIndicator()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ภาพประกอบ",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: Column(children: [
                    previewVideo
                        ? Stack(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Container(
                                  height: 230,
                                  child: _videoController.value.isInitialized
                                      ? AspectRatio(
                                          aspectRatio: _videoController
                                              .value.aspectRatio,
                                          child: VideoPlayer(_videoController),
                                        )
                                      : SizedBox(),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 190, left: 15),
                                child: Container(
                                  height: 35,
                                  width: 35,
                                  child: FloatingActionButton(
                                    onPressed: () {
                                      setState(() {
                                        _videoController.value.isPlaying
                                            ? _videoController.pause()
                                            : _videoController.play();
                                      });
                                    },
                                    child: Icon(
                                      _videoController.value.isPlaying
                                          ? Icons.pause
                                          : Icons.play_arrow,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        : SizedBox(),
                    previewImage
                        ? Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: Image(
                              image: NetworkImage(_imagePreview),
                            ),
                          )
                        : SizedBox(),
                    progressShow
                        ? CircularProgressIndicator()
                        : SizedBox(
                            height: 0,
                          ),
                  ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "ข้อความจากผู้ส่ง",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.black,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      _note,
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.grey,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                _eventStatus == "init"
                    ? Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 20),
                        height: 56.0,
                        decoration: BoxDecoration(
                          color: HexColor.fromHex('#1E62AD'),
                          borderRadius: BorderRadius.circular(28),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2,
                              offset: Offset(1, 1), // Shadow position
                            ),
                          ],
                        ),
                        child: TextButton(
                          onPressed: () {
                            api
                                .workAccept(widget.caseId, CurrentUserId,
                                    GlobalLat, GlobalLong)
                                .then((value) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => TechnicianTracking(
                                            workid: widget.caseId,
                                          )));
                            }).onError((error, stackTrace) {
                              showAlertDialog(context, "");
                            });
                          },
                          child: Text(
                            "รับเรื่อง",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 20,
                      ),
                _eventStatus == "init"
                    ? Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 10),
                        height: 56.0,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.red),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(28),
                        ),
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => TechnicianCancelWork(
                                        workId: widget.caseId)));
                          },
                          child: Text(
                            "ปฎิเสธ",
                            style: TextStyle(color: Colors.red, fontSize: 16),
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 20,
                      ),
              ],
            ),
          ),
        ));
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("ไม่สามารถรับงานได้"),
      content: Text("เนื่องจากมีการรับงานนี้ไปแล้ว"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
