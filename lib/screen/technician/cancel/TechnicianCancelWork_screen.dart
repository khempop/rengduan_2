import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/worklist/TechnicianWorkList_screen.dart';

class TechnicianCancelWork extends StatefulWidget {
  final String workId;
  const TechnicianCancelWork({Key? key, required this.workId})
      : super(key: key);

  @override
  _TechnicianCancelWorkState createState() => _TechnicianCancelWorkState();
}

class _TechnicianCancelWorkState extends State<TechnicianCancelWork> {
  bool isSelectedFile = false;
  String dropdownValue = 'เลือกเหตุผล';
  final _noteController = TextEditingController();

  Future<void> rejectWork() async {
    var api = ApiProvider();
    api.workReject(widget.workId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'ยกเลิก',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  height: 140,
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        Image(
                          height: 100,
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/car_cancel.png"),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 15, top: 8),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Text(
                                    "ยกเลิกรับเรื่องบริการ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: SizedBox(
                                  width: double.infinity,
                                  child:
                                      Text("โปรดระบุข้อมูลให้แก่ผู้รับบริการ",
                                          style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey,
                                            fontSize: 18,
                                          )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
                ),
                Card(
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          child: Row(
                            children: [
                              Text(
                                "เหตุผลการยกเลิก",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              Text(
                                '*',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10.0, right: 10.0, top: 0, bottom: 0),
                      child: Container(
                        height: 54.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: DropdownButton<String>(
                          value: dropdownValue,
                          icon: const Icon(Icons.arrow_drop_down),
                          iconSize: 40,
                          elevation: 16,
                          isExpanded: true,
                          style: const TextStyle(color: Colors.white),
                          underline: Container(
                            height: 2,
                            color: Colors.white,
                          ),
                          onChanged: (String? newValue) {
                            setState(() {
                              dropdownValue = newValue!;
                            });
                          },
                          items: <String>[
                            'เลือกเหตุผล',
                            'ยังไม่มีคิวว่าง',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 11, right: 10),
                                child: Text(
                                  value,
                                  style: TextStyle(
                                      fontFamily: "NotoSans",
                                      color: Colors.grey,
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal),
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          child: Text(
                            "ข้อความถึงเจ้าหน้าที่ (กรุณาระบุเหตุผล)",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(11.0),
                      child: Container(
                        height: 150,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        constraints: BoxConstraints(maxHeight: 200),
                        child: TextField(
                          textInputAction: TextInputAction.done,
                          controller: _noteController,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          keyboardType: TextInputType.multiline,
                          maxLines: 50,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10.0),
                              border: null,
                              hintText: ""),
                        ),
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          margin: EdgeInsets.all(20),
          height: 56.0,
          decoration: BoxDecoration(
            color: HexColor.fromHex('#1E62AD'),
            borderRadius: BorderRadius.circular(28),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2,
                offset: Offset(1, 1), // Shadow position
              ),
            ],
          ),
          child: TextButton(
            onPressed: () {
              rejectWork().then((value) {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (context) => TechnicianWorkList()),
                    (Route<dynamic> route) => false);
              });
            },
            child: Text(
              "ยืนยันการยกเลิก",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
