import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/worklist/TechnicianWorkList_screen.dart';
import 'package:video_player/video_player.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;

class TechnicianComplete extends StatefulWidget {
  final String workId;
  const TechnicianComplete({Key? key, required this.workId}) : super(key: key);

  @override
  _TectnicianCompleteState createState() => _TectnicianCompleteState();
}

enum FileType { image, video, none }

class _TectnicianCompleteState extends State<TechnicianComplete> {
  final _picker = ImagePicker();
  File? selectedFile;
  bool isSelectedFile = false;
  late VideoPlayerController _controller;
  FileType fileType = FileType.none;
  var progressShow = false;
  String fileAttachUrl = "";
  String workId = "";
  var fileAttachUrlComplate = false;
  final _noteController = TextEditingController();
  double _rating = 0.0;

  /* _selectFromGallery(FileType fileType) async {
    this.fileType = fileType;
    try {
      if (fileType == FileType.image) {
        final pickedFile = await _picker.getImage(source: ImageSource.camera);

        if (pickedFile != null) {
          setState(() {
            print("file path " + pickedFile.path);
            selectedFile = File(pickedFile.path);
            isSelectedFile = true;
          });
          uploadFileImg(pickedFile.path);
        }
      } else if (fileType == FileType.video) {
        final pickedFile = await _picker.getVideo(
            source: ImageSource.camera, maxDuration: Duration(seconds: 10));
        if (pickedFile != null) {
          setState(() {
            print("file path " + pickedFile.path);
            selectedFile = File(pickedFile.path);
            isSelectedFile = true;
          });
          uploadFileVideo(pickedFile.path);
        }
      }
    } catch (e) {
      setState(() {});
    }
  }

  Future<void> uploadFileImg(String filePath) async {
    File file = File(filePath);

    setState(() {
      progressShow = true;
    });
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('image/image$workId.jpg')
          .putFile(file);

      String downloadURL = await firebase_storage.FirebaseStorage.instance
          .ref('image/image$workId.jpg')
          .getDownloadURL();
      print("url $downloadURL");

      setState(() {
        fileAttachUrl = downloadURL;
        progressShow = false;
        fileAttachUrlComplate = true;
      });
    } on firebase_core.FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }*/

/*  Future<void> uploadFileVideo(String filePath) async {
    File file = File(filePath);

    print(filePath);
    setState(() {
      progressShow = true;
    });
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('video/file$workId.mp4')
          .putFile(file);

      String downloadURL = await firebase_storage.FirebaseStorage.instance
          .ref('video/file$workId.mp4')
          .getDownloadURL();
      print("url $downloadURL");
      setState(() {
        setState(() {
          fileAttachUrl = downloadURL;
          progressShow = false;
          fileAttachUrlComplate = true;
        });
      });

      _controller = VideoPlayerController.network(fileAttachUrl)
        ..initialize().then((_) {
          setState(() {});
        });
    } on firebase_core.FirebaseException catch (e) {}
  }*/

  Future<void> closeWork() async {
    var api = ApiProvider();
    api.workComplete(workId, "", "", "", "", "", "", "0", "");
  }

  @override
  void initState() {
    workId = widget.workId;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ProgressHUD(
      child: Builder(
        builder: (context) => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black, //change your color here
            ),
            backgroundColor: Colors.white,
            elevation: 0.0,
            title: Column(
              children: [
                Container(
                  child: Text(
                    'สำเร็จ!',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Container(
                      height: 140,
                      child: Card(
                          child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          children: [
                            Image(
                              height: 100,
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/car_green.png"),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 15, top: 8),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        "สิ้นสุดการให้บริการ",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                    ),
                  ],
                ),
              ),
            ),
          ),
          bottomNavigationBar: BottomAppBar(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.90,
              margin: EdgeInsets.all(20),
              height: 56.0,
              decoration: BoxDecoration(
                color: HexColor.fromHex('#1E62AD'),
                borderRadius: BorderRadius.circular(28),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2,
                    offset: Offset(1, 1), // Shadow position
                  ),
                ],
              ),
              child: TextButton(
                onPressed: () {
                  print("ddddd");
                  final progress = ProgressHUD.of(context);
                  progress!.show();
                  closeWork().then((value) {
                    progress.dismiss();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => TechnicianWorkList()),
                        (Route<dynamic> route) => false);
                  }).onError((error, stackTrace) {
                    progress.dismiss();
                    TechnicianWorkList();
                  });
                },
                child: Text(
                  "ตกลง / Submit",
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
