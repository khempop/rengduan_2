class Cart {
  final double defaultRate = 30.00;
  double getDefaultRate() {
    return defaultRate;
  }

  double amount() {
    return defaultRate;
  }
}
