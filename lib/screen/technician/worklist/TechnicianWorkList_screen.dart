import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rengduan/Extension/DatetimeFormart.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/deposit_point/deposit_point.dart';
import 'package:rengduan/screen/technician/edit_profile/EditProfile_screen.dart';
import 'package:rengduan/screen/technician/tracking/TechnicianTracking_screen.dart';
import 'package:rengduan/screen/technician/work_detail/WorkDetail_screen.dart';

class TechnicianWorkList extends StatefulWidget {
  const TechnicianWorkList({Key? key}) : super(key: key);

  @override
  _TechnicianWorkListState createState() => _TechnicianWorkListState();
}

class _TechnicianWorkListState extends State<TechnicianWorkList> {
  List<WorkOrderModel> _workList = [];
  bool stopInterval = false;
  String _credit = "";
  @override
  void initState() {
    Timer.periodic(new Duration(seconds: 5), (timer) {
      if (stopInterval) {
        timer.cancel();
      }
      getWorkList();
      getUserInfo();
    });

    super.initState();
  }

  getUserInfo() async {
    String uid = await SharedPreferencesHelper.getUserID();
    var api = ApiProvider();
    api.fetchUserInfoById(uid).then((value) {
      setState(() {
        _credit = value.credit;
      });
    });
  }

  getWorkList() async {
    String uid = await SharedPreferencesHelper.getUserID();
    var api = ApiProvider();
    api.getTechnicainWorkList(uid).then((value) {
      setState(() {
        _workList = value;
      });

      //   _workList.forEach((element) {
      //     if (element.servicestatus == "accept") {
      //       stopInterval = true;
      //       Navigator.push(
      //           context,
      //           MaterialPageRoute(
      //               builder: (_) => TechnicianTracking(
      //                     workid: element.uid,
      //                   )));
      //     }
      //   });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Scaffold(
          body: Stack(
            children: [
              Image(
                width: double.infinity,
                image: AssetImage("assets/images/bg_appbar.png"),
              ),
              AppBar(
                iconTheme: IconThemeData(
                  color: Colors.white, //change your color here
                ),
                elevation: 0,
                backgroundColor: Colors.transparent,
                title: Column(
                  children: [
                    Container(
                      child: Text(
                        'ช่าง',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        'PERSONAL INDENTIFICATION',
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => TechnicianEditProfile()));
                      },
                      child: CircleAvatar(
                          backgroundImage: NetworkImage(UserModel.imageUrl)),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(top: 80),
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: Colors.white.withOpacity(0.8),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text(_credit,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 45,
                                      )),
                                  subtitle: Text("point",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 18,
                                      )),
                                  trailing: IconButton(
                                    icon: Icon(
                                      Icons.add_circle_rounded,
                                      color: Colors.grey,
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => DepositPoint()));
                                    },
                                    iconSize: 50,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.white,
                    ),
                    child: Container(
                      height: 350,
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20, left: 20),
                              child: Text("ประวัติการให้บริการ",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 18,
                                  )),
                            ),
                          ),
                          SingleChildScrollView(
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.55,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ListView.builder(
                                    itemCount: _workList.length,
                                    itemBuilder: (context, index) {
                                      return Card(
                                          child: ListTile(
                                              onTap: () {
                                                if (GlobalLat == "" ||
                                                    GlobalLong == "") {
                                                  showAlertDialog(context, "");
                                                }

                                                if (_workList[index]
                                                            .servicestatus ==
                                                        "complete" ||
                                                    _workList[index]
                                                            .servicestatus ==
                                                        "reject") {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) =>
                                                              WorkDetail(
                                                                caseId: _workList[
                                                                        index]
                                                                    .uid,
                                                              )));
                                                } else if (_workList[index]
                                                        .servicestatus ==
                                                    "accept") {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) =>
                                                              TechnicianTracking(
                                                                workid: _workList[
                                                                        index]
                                                                    .uid,
                                                              )));
                                                } else {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (_) =>
                                                              WorkDetail(
                                                                caseId: _workList[
                                                                        index]
                                                                    .uid,
                                                              )));
                                                }
                                              },
                                              title: Text(
                                                _workList[index].caseid,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                  fontSize: 18,
                                                ),
                                              ),
                                              subtitle: Text(
                                                DatetimeFormat.formatDatetime(
                                                    _workList[index]
                                                        .requesttime),
                                                style: TextStyle(
                                                  fontWeight: FontWeight.normal,
                                                  color: Colors.black,
                                                  fontSize: 14,
                                                ),
                                              ),
                                              trailing: statusWork(
                                                  _workList[index]
                                                      .servicestatus,
                                                  _workList[index]
                                                      .userstarrating)));
                                    }),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget statusWork(String working, String completeRating) {
    if (completeRating == "") {
      completeRating = "0";
    }
    if (working == "complete") {
      return RatingBar.builder(
        ignoreGestures: true,
        itemSize: 18,
        initialRating: double.parse(completeRating),
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
        itemBuilder: (context, _) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        onRatingUpdate: (rating) {
          print(rating);
        },
      );
    } else {
      Color color = HexColor.fromHex("F1621E");
      String text = "";
      switch (working) {
        case "reject":
          {
            color = Colors.red;
            text = "ยกเลิก";
            break;
          }
        case "init":
          {
            color = Colors.green;
            text = "งานมาใหม่";
            break;
          }
        case "accept":
          {
            color = HexColor.fromHex("F1621E");
            text = "กำลังดำเนินการ";
            break;
          }
      }
      return DecoratedBox(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: Colors.white,
              fontSize: 14,
            ),
          ),
        ),
      );
    }
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
        requestPermisLocation().then((value) {});
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("ไม่สามารถใช้บริการได้"),
      content: Text("ต้องระบุ location ของคุณก่อน"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> requestPermisLocation() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled == false) {
      await openAppSettings();
    }
  }
}
