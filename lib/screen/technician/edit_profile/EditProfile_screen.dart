import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/login_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/verify/TechnicainVerify_screen.dart';
import 'package:intl/intl.dart';

import '../../../Constant.dart';

class TechnicianEditProfile extends StatefulWidget {
  const TechnicianEditProfile({Key? key}) : super(key: key);

  @override
  _TechnicianEditProfilState createState() => _TechnicianEditProfilState();
}

class _TechnicianEditProfilState extends State<TechnicianEditProfile> {
  bool _checkbox = false;
  String dropdownValue = 'ช่างรถยนต์';
  String dropdownValueId = "01";

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  TextEditingController _controllerFName = new TextEditingController();
  TextEditingController _controllerLName = new TextEditingController();
  TextEditingController _controllerEmail = new TextEditingController();
  TextEditingController _controllerTel = new TextEditingController();
  String _imageUrl = "";
  String _uid = "";
  bool user = true;
  @override
  void initState() {
    var api = ApiProvider();
    SharedPreferencesHelper.getUserID().then((uid) {
      api.fetchUserInfoById(uid).then((value) {
        print(value.servicetype);

        setState(() {
          _controllerFName = TextEditingController(text: value.name);
          _controllerLName = TextEditingController(text: value.surname);
          _controllerEmail = TextEditingController(text: value.email);
          _controllerTel = TextEditingController(text: value.phone);
          dropdownValueId = value.servicetype;
          dropdownValue = WorkTypeNameList[value.servicetype];
          _imageUrl = value.selfieUrl;
          _checkbox = true;
          _uid = value.uid;

          if (value.usertype == "user") {
            user = false;
          }
        });
      });
    });

    super.initState();
  }

  Future<void> addUser(BuildContext context) {
    // Call the user's CollectionReference to add a new user
    String tel = _controllerTel.text;
    print(tel);

    ApiProvider service = ApiProvider();
    Map userMap = {
      "uid": _uid,
      "name": _controllerFName.text,
      "surname": _controllerLName.text,
      "email": _controllerEmail.text,
      "phone": tel,
      "selfie_url": _imageUrl,
      "createtime": DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()),
      "servicetype": WorkTypeMapIDList[dropdownValue],
      "usertype": "provider",
      "userstatus": "inactive",
      "credit": "0",
      "agreement": "yes"
    };

    return service.updateProviderInfo(userMap, context).then((value) {
      setUserDefault(value);
    });
  }

  Future<void> setUserDefault(UserInfomation user) async {
    await SharedPreferencesHelper.setUserID(user.uid);
    await SharedPreferencesHelper.setUserEmail(user.email);
    await SharedPreferencesHelper.setUserTel(user.phone);
    await SharedPreferencesHelper.setUserName(user.name);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.replay_circle_filled_rounded),
              tooltip: 'reload',
              onPressed: () {
                setState(() {});
                ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Reload profile')));
              },
            )
          ],
          elevation: 0.0,
          title: Column(
            children: [
              Container(
                child: Text(
                  'ระบุข้อมูลผู้ใช้',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                child: Text(
                  'USER INFORMATION',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
        body: ProgressHUD(
            child: Builder(
          builder: (context) => SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'ชื่อ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerFName,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุชื่อ'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'นามสกุล',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerLName,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุนามสกุล'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'เบอร์โทรศัพท์',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerTel,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'เบอร์โทรศัพท์'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'อีเมล',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุอีเมล'),
                  ),
                ),
                Visibility(
                  visible: user,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, top: 10),
                    child: SizedBox(
                      width: double.infinity,
                      child: Container(
                        child: Row(
                          children: [
                            Text(
                              'ประเภทช่าง *',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              '*',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: user,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 0, bottom: 0),
                    child: Container(
                      height: 54.0,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: DropdownButton<String>(
                        value: dropdownValue,
                        icon: const Icon(Icons.arrow_drop_down),
                        iconSize: 40,
                        elevation: 16,
                        isExpanded: true,
                        style: const TextStyle(color: Colors.white),
                        underline: Container(
                          height: 2,
                          color: Colors.white,
                        ),
                        onChanged: (String? newValue) {
                          setState(() {
                            // print(newValue);
                            //  print(WorkTypeMapIDList[newValue]);
                            // dropdownValue = newValue!;
                          });
                        },
                        items: WorkTypeList.map<DropdownMenuItem<String>>(
                            (String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 11, right: 10),
                              child: Text(
                                value,
                                style: TextStyle(
                                    fontFamily: "NotoSans",
                                    color: Colors.grey,
                                    fontSize: 16,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ),
                /*Container(
                  width: MediaQuery.of(context).size.width * 0.90,
                  margin: EdgeInsets.all(20),
                  height: 56.0,
                  decoration: BoxDecoration(
                    color:
                        _checkbox ? HexColor.fromHex('#1E62AD') : Colors.grey,
                    borderRadius: BorderRadius.circular(28),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2,
                        offset: Offset(1, 1), // Shadow position
                      ),
                    ],
                  ),
                  child: TextButton(
                    onPressed: () {
                      if (_checkbox) {
                        addUser(context).then((value) {
                          Navigator.pop(context);
                        });
                      }
                    },
                    child: Text(
                      "แก้ไข / Edit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),*/
                Container(
                  width: MediaQuery.of(context).size.width * 0.90,
                  margin: EdgeInsets.all(20),
                  height: 56.0,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(28),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2,
                        offset: Offset(1, 1), // Shadow position
                      ),
                    ],
                  ),
                  child: TextButton(
                    onPressed: () {
                      //if (_checkbox) {
                      signOut();
                      //}
                    },
                    child: Text(
                      "ออกจากระบบ",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
        )));
  }

  signOut() {
    GoogleSignIn().signOut();
    FirebaseAuth.instance.signOut();
    SharedPreferencesHelper.setUserID("");
    SharedPreferencesHelper.setUserEmail("");
    SharedPreferencesHelper.setUserTel("");
    _controllerFName = TextEditingController(text: "");
    _controllerLName = TextEditingController(text: "");
    _controllerEmail = TextEditingController(text: "");
    _controllerTel = TextEditingController(text: "");
    dropdownValueId = "";
    _imageUrl = "";
    _checkbox = true;
    _uid = "";
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => Login()),
        (Route<dynamic> route) => false);
  }
}
