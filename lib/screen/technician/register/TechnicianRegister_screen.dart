import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/technician/verify/TechnicainVerify_screen.dart';
import 'package:intl/intl.dart';

import '../../../Constant.dart';

class TechnicianRegister extends StatefulWidget {
  const TechnicianRegister({Key? key}) : super(key: key);

  @override
  _TechnicianRegisterState createState() => _TechnicianRegisterState();
}

class _TechnicianRegisterState extends State<TechnicianRegister> {
  bool _checkbox = false;
  String dropdownValue = 'ช่างกุญแจ';
  String dropdownValueId = "01";

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  TextEditingController _controllerFName = new TextEditingController();
  TextEditingController _controllerLName = new TextEditingController();
  TextEditingController _controllerEmail = new TextEditingController();
  TextEditingController _controllerTel = new TextEditingController();
  String _term = "";
  @override
  void initState() {
    super.initState();

    print("_locationData.latitude $GlobalLat");

    var splitag = UserModel.profileName.split(" ");
    _controllerFName = new TextEditingController(text: splitag[0]);
    if (splitag.length == 2) {
      _controllerLName = new TextEditingController(text: splitag[1]);
    }
    _controllerEmail = new TextEditingController(text: UserModel.email);

    loadAssetTerm().then((value) {
      setState(() {
        _term = value;
      });
    });
  }

  Future<void> requestPermisLocation() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled == false) {
      await openAppSettings();
    }
  }

  Future<void> addUser(BuildContext context) {
    // Call the user's CollectionReference to add a new user
    String tel = _controllerTel.text;
    GlobalWorkType = WorkTypeMapIDList[dropdownValue];
    ApiProvider service = ApiProvider();
    Map userMap = {
      "uid": "",
      "name": _controllerFName.text,
      "surname": _controllerLName.text,
      "email": _controllerEmail.text,
      "phone": tel,
      "selfie_url": "",
      "createtime": DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()),
      "servicetype": WorkTypeMapIDList[dropdownValue],
      "usertype": "provider",
      "userstatus": "inactive",
      "credit": "0",
      "agreement": "yes",
      "profile_image": UserModel.imageUrl
    };
    return service.saveProviderInfo(userMap, context).then((value) {
      setUserDefault(value);
    });
  }

  Future<void> setUserDefault(UserInfomation user) async {
    await SharedPreferencesHelper.setUserID(user.uid);
    await SharedPreferencesHelper.setUserEmail(user.email);
    await SharedPreferencesHelper.setUserTel(user.phone);
    await SharedPreferencesHelper.setUserName(user.name);
    addUserLocation(user.uid);
  }

  Future<void> addUserLocation(String userid) {
    CollectionReference users =
        FirebaseFirestore.instance.collection('users_location');
    //print("update location");
    return users.doc(userid).set({
      'uid': userid,
      'lat': GlobalLat,
      'long': GlobalLong,
      'servicetype': GlobalWorkType,
      'token': GlobalToken
    }).then((value) {
      print("update token $GlobalToken  lat $GlobalLat");
    }).catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Column(
            children: [
              Container(
                child: Text(
                  'ระบุข้อมูลผู้ใช้',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                child: Text(
                  'USER INFORMATION',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
        body: ProgressHUD(
            child: Builder(
          builder: (context) => SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'ชื่อ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerFName,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุชื่อ'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'นามสกุล',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerLName,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุนามสกุล'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'เบอร์โทรศัพท์',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerTel,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'เบอร์โทรศัพท์'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'อีเมล',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerEmail,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'ระบุอีเมล'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'ประเภทช่าง *',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: Container(
                    height: 54.0,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 40,
                      elevation: 16,
                      isExpanded: true,
                      style: const TextStyle(color: Colors.white),
                      underline: Container(
                        height: 2,
                        color: Colors.white,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: WorkTypeList.map<DropdownMenuItem<String>>(
                          (String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 11, right: 10),
                            child: Text(
                              value,
                              style: TextStyle(
                                  fontFamily: "NotoSans",
                                  color: Colors.grey,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'Terms and Conditions',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10, right: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    constraints: BoxConstraints(maxHeight: 200),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          _term,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: SizedBox(
                        width: double.infinity,
                        child: Row(
                          children: [
                            Checkbox(
                              value: _checkbox,
                              onChanged: (value) {
                                if (GlobalLat == "") {
                                  showAlertDialog(context, "");
                                }
                                setState(() {
                                  _checkbox = !_checkbox;
                                });
                              },
                            ),
                            Text(
                              'ยอมรับเงื่อนไขการบริการ RengDuan',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.90,
                  margin: EdgeInsets.all(20),
                  height: 56.0,
                  decoration: BoxDecoration(
                    color:
                        _checkbox ? HexColor.fromHex('#1E62AD') : Colors.grey,
                    borderRadius: BorderRadius.circular(28),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2,
                        offset: Offset(1, 1), // Shadow position
                      ),
                    ],
                  ),
                  child: TextButton(
                    onPressed: () {
                      if (_checkbox) {
                        addUser(context).then((value) {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => TechnicianVerify()),
                              (Route<dynamic> route) => false);
                        });
                      }
                    },
                    child: Text(
                      "ถัดไป /  Next",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
        )));
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
        requestPermisLocation().then((value) {});
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("ไม่สามารถใช้บริการได้"),
      content: Text("ต้องระบุ location ของคุณก่อน"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
