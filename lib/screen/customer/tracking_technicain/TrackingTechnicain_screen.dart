import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/screen/customer/complete_work/UserCompleteWrok_screen.dart';
import 'dart:async';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/select_work_type/select_worktype_screen.dart';
import 'package:rengduan/screen/customer/waiiting_technicain/WaitingTechnicain_screen.dart';

import '../../../Constant.dart';

class TrackingTechnician extends StatefulWidget {
  final String workid;
  const TrackingTechnician({Key? key, required this.workid}) : super(key: key);

  @override
  _TrackingTechnicianState createState() => _TrackingTechnicianState();
}

class _TrackingTechnicianState extends State<TrackingTechnician> {
  BitmapDescriptor? _markerIcon;
  BitmapDescriptor? _markerIcon2;
  Completer<GoogleMapController> _controller = Completer();

  String _eventId = "";

  double providerLat = 18.7597;
  double providerLong = 98.95154;

  double userLat = 18.7597;
  double userLong = 98.95154;
  bool _mapStart = false;
  var api = ApiProvider();
  String _userAddress = "";
  String _providerAddress = "";

  String _providerUID = "";
  String _providerName = "-";
  String _providerType = "-";
  String _providerImage =
      "https://firebasestorage.googleapis.com/v0/b/rengduan-2.appspot.com/o/user%2Fverify%2Fcd7a4df4-246e-465b-abac-f9d6d1ff7753.png?alt=media&token=a22836e2-3166-476c-8499-9a769e1a2fc9";
  String _duration = "";

  //add your lat and lng where you wants to draw polyline
  PolylinePoints polylinePoints = PolylinePoints();
  Map<PolylineId, Polyline> polylines = {};

  @override
  void initState() {
    _eventId = widget.workid;

    api.getWorkingStatus(_eventId).then((value) {
      setState(() {
        _providerUID = value.providerid;
        WorkRequestModel.lat = value.userlat;
        WorkRequestModel.long = value.userlong;
        userLat = double.parse(value.userlat);
        userLong = double.parse(value.userlong);

        _getPolyline(
            PointLatLng(double.parse(value.providerlat),
                double.parse(value.providerlong)),
            PointLatLng(
                double.parse(value.userlat), double.parse(value.userlong)));
        updateUserLatLong();
      });
      getTechicainDetail(value.providerid, value.servicetype);
      getData();
    });
    Timer.periodic(new Duration(seconds: 3), (timer) {
      getStatus(timer);
    });
    super.initState();
  }

  void getTechicainDetail(providerid, serviceType) {
    api.fetchUserInfoById(providerid).then((user) {
      setState(() {
        _providerName = user.name + " " + user.surname;

        _providerType = WorkTypeNameList[serviceType];
        _providerImage = user.profileImage;
      });
    });
  }

  void _getPolyline(PointLatLng start, PointLatLng end) async {
    List<LatLng> polylineCoordinates = [];
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      ApiProvider.MAP_API_KEY,
      start,
      end,
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
      _mapStart = true;
    } else {
      print(result.errorMessage);
    }
    _addPolyLine(polylineCoordinates);
  }

  _addPolyLine(List<LatLng> polylineCoordinates) {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.blueAccent,
      points: polylineCoordinates,
      width: 8,
    );
    polylines[id] = polyline;
    setState(() {});
  }

  getStatus(timer) {
    var api = ApiProvider();
    api.getWorkingStatus(widget.workid).then((value) {
      if (value.servicestatus == "reject") {
        timer.cancel();

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => SelectWorkType()),
            (Route<dynamic> route) => false);
      }

      if (value.servicestatus == "complete") {
        WorkOrder = value;
        timer.cancel();

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => UserCompleteWork(workid: value.uid)),
            (Route<dynamic> route) => false);
      }
    });
  }

  updateUserLatLong() {
    api
        .getPlacesFromGoogle(WorkRequestModel.lat, WorkRequestModel.long)
        .then((value) {
      setState(() {
        _userAddress = value.addressName;
      });
    });
  }

  Future<dynamic> getData() async {
    final DocumentReference document = FirebaseFirestore.instance
        .collection("users_location")
        .doc(_providerUID);

    document.snapshots().listen((event) {
      print(event["lat"].toString());
      setState(() {
        _mapStart = true;
        providerLat = double.parse(event["lat"].toString());
        providerLong = double.parse(event["long"].toString());
        print("providerLat  $providerLat ,  $providerLong");
      });

      api
          .getPlacesFromGoogle(
              event["lat"].toString(), event["long"].toString())
          .then((value) {
        setState(() {
          _providerAddress = value.addressName;
        });
      });

      api
          .getDurationFromGoogle(
              event["lat"].toString(),
              event["long"].toString(),
              WorkRequestModel.lat,
              WorkRequestModel.long)
          .then((value) {
        setState(() {
          _duration = value.duration;
        });
        var aa = event["lat"].toString();
        var bb = event["long"].toString();
        //   print("between $aa  $bb  ");
        _getPolyline(
            PointLatLng(double.parse(event["lat"].toString()),
                double.parse(event["long"].toString())),
            PointLatLng(double.parse(WorkRequestModel.lat),
                double.parse(WorkRequestModel.long)));
      });
    });
  }

  Marker _createMarker() {
    if (_markerIcon != null) {
      return Marker(
        markerId: MarkerId("marker_1"),
        position: LatLng(providerLat, providerLong),
        icon: _markerIcon!,
      );
    } else {
      return Marker(
        markerId: MarkerId("marker_1"),
        position: LatLng(providerLat, providerLong),
      );
    }
  }

  Marker _createMarker2() {
    if (_markerIcon != null) {
      return Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(userLat, userLong),
        icon: _markerIcon2!,
      );
    } else {
      return Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(userLat, userLong),
      );
    }
  }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size.square(20));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/images/marker_start.png')
          .then(_updateBitmap);
    }
  }

  Future<void> _createMarkerImageFromAsset2(BuildContext context) async {
    if (_markerIcon2 == null) {
      final ImageConfiguration imageConfiguration =
          createLocalImageConfiguration(context, size: Size.square(20));
      BitmapDescriptor.fromAssetImage(
              imageConfiguration, 'assets/images/marker_des.png')
          .then(_updateBitmap2);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    print("_updateBitmap1");
    setState(() {
      _markerIcon = bitmap;
    });
  }

  void _updateBitmap2(BitmapDescriptor bitmap) {
    print("_updateBitmap2");
    setState(() {
      _markerIcon2 = bitmap;
    });
  }

  @override
  Widget build(BuildContext context) {
    _createMarkerImageFromAsset(context);
    _createMarkerImageFromAsset2(context);
    return new Scaffold(
      body: Stack(
        children: [
          _mapStart
              ? GoogleMap(
                  // markers: Set<Marker>.of(_markers),
                  markers: <Marker>{_createMarker(), _createMarker2()},
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: LatLng(providerLat, providerLong),
                    zoom: 15,
                  ),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  polylines: Set<Polyline>.of(polylines.values),
                )
              : CircularProgressIndicator(),
          Align(
            alignment: FractionalOffset.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: Container(
                height: 350,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  color: Colors.white,
                ),
                child: Container(
                  height: 350,
                  child: Column(
                    children: [
                      Card(
                          child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 50,
                              backgroundImage: NetworkImage(_providerImage),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 15, top: 8),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        _providerName,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Text(_providerType,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey,
                                            fontSize: 18,
                                          )),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: 200,
                                          child: Text(_duration,
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black,
                                                fontSize: 18,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                      Container(
                        child: ListTile(
                          /*leading: Image(
                            height: 50,
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/worktype2.png"),
                          ),*/
                          leading: Icon(
                            Icons.person,
                            color: HexColor.fromHex('#1E62AD'),
                          ),
                          title: Text(_providerAddress,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey,
                                fontSize: 15,
                              )),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 25),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            height: 50,
                            width: 4,
                            child: DecoratedBox(
                              decoration: BoxDecoration(
                                color: HexColor.fromHex('#1E62AD'),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: ListTile(
                          /*leading: Image(
                            height: 50,
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/worktype2.png"),
                          ),*/
                          leading: Icon(
                            Icons.pin_drop,
                            color: HexColor.fromHex('#1E62AD'),
                          ),
                          title: Text(_userAddress,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.grey,
                                fontSize: 15,
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
