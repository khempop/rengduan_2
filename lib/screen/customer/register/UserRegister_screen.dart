import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:rengduan/Constant.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/screen/customer/into/into_screen.dart';
import 'package:rengduan/screen/customer/verify/user_verify_screen.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:intl/intl.dart';

class UserRegister extends StatefulWidget {
  const UserRegister({Key? key}) : super(key: key);

  @override
  _UserRegisterState createState() => _UserRegisterState();
}

class _UserRegisterState extends State<UserRegister> {
  bool _checkbox = false;
  bool _gooleSignIn = true;

  TextEditingController _controllerFName = new TextEditingController();
  TextEditingController _controllerLName = new TextEditingController();
  TextEditingController _controllerEmail = new TextEditingController();
  TextEditingController _controllerTel = new TextEditingController();

  String _term = "";
  @override
  void initState() {
    super.initState();

    var splitag = UserModel.profileName.split(" ");
    _controllerFName = new TextEditingController(text: splitag[0]);
    if (splitag.length == 2) {
      _controllerLName = new TextEditingController(text: splitag[1]);
    }
    _controllerEmail = new TextEditingController(text: UserModel.email);

    loadAssetTerm().then((value) {
      setState(() {
        if (UserModel.appleSign) {
          _gooleSignIn = false;
        }
        _term = value;
      });
    });
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void> addUser(context) {
    String tel = _controllerTel.text;
    print(tel);

    ApiProvider service = ApiProvider();
    Map userMap = {
      "uid": "",
      "name": _controllerFName.text,
      "surname": _controllerLName.text,
      "email": _controllerEmail.text,
      "phone": tel,
      "selfie_url": "",
      "createtime": DateFormat('yyyy-MM-dd hh:mm:ss').format(DateTime.now()),
      "servicetype": "",
      "usertype": "user",
      "userstatus": "active",
      "credit": "0",
      "agreement": "yes",
      "profile_image": UserModel.imageUrl
    };
    print(userMap);
    return service.saveUserInfo(userMap, context).then((value) {
      setUserDefault(value);
    });
  }

  Future<void> setUserDefault(UserInfomation user) async {
    await SharedPreferencesHelper.setUserID(user.uid);
    await SharedPreferencesHelper.setUserEmail(user.email);
    await SharedPreferencesHelper.setUserTel(user.phone);
    await SharedPreferencesHelper.setUserName(user.name);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Column(
            children: [
              Container(
                child: Text(
                  'ระบุข้อมูลผู้ใช้',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                child: Text(
                  'USER INFORMATION',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
        backgroundColor: Colors.white,
        body: ProgressHUD(
            child: Builder(
          builder: (context) => SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Visibility(
                  visible: _gooleSignIn,
                  child: Column(children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 10),
                      child: SizedBox(
                        width: double.infinity,
                        child: Container(
                          child: Row(
                            children: [
                              Text(
                                'ชื่อ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              Text(
                                '*',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 0, bottom: 0),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        controller: _controllerFName,
                        decoration: InputDecoration(
                            isDense: true, // Added this
                            contentPadding: EdgeInsets.all(14),
                            border: OutlineInputBorder(),
                            // labelText: 'Email',
                            hintText: 'ระบุชื่อ'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 10),
                      child: SizedBox(
                        width: double.infinity,
                        child: Container(
                          child: Row(
                            children: [
                              Text(
                                'นามสกุล',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              Text(
                                '*',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 0, bottom: 0),
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        controller: _controllerLName,
                        decoration: InputDecoration(
                            isDense: true, // Added this
                            contentPadding: EdgeInsets.all(14),
                            border: OutlineInputBorder(),
                            // labelText: 'Email',
                            hintText: 'ระบุนามสกุล'),
                      ),
                    ),
                  ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'เบอร์โทรศัพท์',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                          Text(
                            '*',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20.0, right: 20.0, top: 0, bottom: 0),
                  child: TextField(
                    textInputAction: TextInputAction.done,
                    controller: _controllerTel,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        isDense: true, // Added this
                        contentPadding: EdgeInsets.all(14),
                        border: OutlineInputBorder(),
                        // labelText: 'Email',
                        hintText: 'เบอร์โทรศัพท์'),
                  ),
                ),
                Visibility(
                  visible: _gooleSignIn,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 10),
                        child: SizedBox(
                          width: double.infinity,
                          child: Container(
                            child: Row(
                              children: [
                                Text(
                                  'อีเมล',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  '*',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.red,
                                    fontSize: 18,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 0, bottom: 0),
                        child: TextField(
                          textInputAction: TextInputAction.done,
                          controller: _controllerEmail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                              isDense: true, // Added this
                              contentPadding: EdgeInsets.all(14),
                              border: OutlineInputBorder(),
                              // labelText: 'Email',
                              hintText: 'ระบุอีเมล'),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: Row(
                        children: [
                          Text(
                            'Terms and Conditions',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10, right: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    constraints: BoxConstraints(maxHeight: 200),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          _term,
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10),
                  child: SizedBox(
                    width: double.infinity,
                    child: Container(
                      child: SizedBox(
                        width: double.infinity,
                        child: Row(
                          children: [
                            Checkbox(
                              value: _checkbox,
                              onChanged: (value) {
                                setState(() {
                                  _checkbox = !_checkbox;
                                });
                              },
                            ),
                            Text(
                              'ยอมรับเงื่อนไขการบริการ RengDuan',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.90,
                  margin: EdgeInsets.all(20),
                  height: 56.0,
                  decoration: BoxDecoration(
                    color:
                        _checkbox ? HexColor.fromHex('#1E62AD') : Colors.grey,
                    borderRadius: BorderRadius.circular(28),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        blurRadius: 2,
                        offset: Offset(1, 1), // Shadow position
                      ),
                    ],
                  ),
                  child: TextButton(
                    onPressed: () {
                      if (_checkbox) {
                        addUser(context).then((value) {
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => IntoUserApp()),
                              (Route<dynamic> route) => false);
                        });
                      }
                    },
                    child: Text(
                      "ถัดไป /  Next",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
        )));
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
