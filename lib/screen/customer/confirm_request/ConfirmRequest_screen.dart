import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:rengduan/Constant.dart';
import 'package:rengduan/Extension/CurrentUser.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/waiiting_technicain/WaitingTechnicain_screen.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:video_player/video_player.dart';
import 'package:permission_handler/permission_handler.dart';

class ConfirmRequest extends StatefulWidget {
  const ConfirmRequest({Key? key}) : super(key: key);

  @override
  _ConfirmRequestState createState() => _ConfirmRequestState();
}

class _ConfirmRequestState extends State<ConfirmRequest> {
  final _picker = ImagePicker();
  File? selectedFile;
  bool isSelectedFile = false;
  bool progressShow = false;
  CollectionReference workOrder =
      FirebaseFirestore.instance.collection('work_order');
  List fileSteam = [];
  var fileAttach = "";
  var docId = "";
  var previewVideo = false;
  var previewImage = false;
  var _imagePreview =
      "https://images.unsplash.com/photo-1547721064-da6cfb341d50";
  var _videoPreview = "";

  WorkOrderModel? _workStatus;
  var _noteTextController = new TextEditingController();
  _imgFromGallery() async {
    print("xxx ");

    try {
      final pickedFile = await _picker.getImage(source: ImageSource.camera);
      print("xxx --- ");
      if (pickedFile != null) {
        setState(() {
          print("file path " + pickedFile.path);
          uploadFileImg(pickedFile.path);
          selectedFile = File(pickedFile.path);
          isSelectedFile = true;
        });
      }
    } catch (e) {
      setState(() {});
    }
  }

  _videoFromGallery() async {
    print("xxx ");
    try {
      final pickedFile = await _picker.getVideo(
          source: ImageSource.camera, maxDuration: Duration(seconds: 10));
      print("xxx --- ");
      if (pickedFile != null) {
        setState(() {
          print("file path " + pickedFile.path);
          uploadFileVideo(pickedFile.path);
          selectedFile = File(pickedFile.path);
          isSelectedFile = true;
        });
      }
    } catch (e) {
      setState(() {});
    }
  }

  late VideoPlayerController _controller;

  @override
  void initState() {
    getLocation();
    super.initState();
  }

  Future<void> getLocation() async {
    Location location = new Location();

    bool _serviceEnabled;
    // PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _locationData = await location.getLocation();
    print("_locationData.latitude");
    print(_locationData.latitude);
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  Future<void> uploadFileImg(String filePath) async {
    File file = File(filePath);

    setState(() {
      progressShow = true;
    });
    try {
      var ts = DateTime.now().millisecondsSinceEpoch;
      await firebase_storage.FirebaseStorage.instance
          .ref('image/image$ts.jpg')
          .putFile(file);

      String downloadURL = await firebase_storage.FirebaseStorage.instance
          .ref('image/image$ts.jpg')
          .getDownloadURL();
      print("url $downloadURL");

      setState(() {
        fileAttach = downloadURL;
        progressShow = false;
        previewImage = true;
        _imagePreview = downloadURL;
        previewVideo = false;
      });
    } on firebase_core.FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  Future<void> uploadFileVideo(String filePath) async {
    File file = File(filePath);

    print(filePath);
    setState(() {
      progressShow = true;
    });
    try {
      var ts = DateTime.now().millisecondsSinceEpoch;
      await firebase_storage.FirebaseStorage.instance
          .ref('video/file$ts.mp4')
          .putFile(file);

      String downloadURL = await firebase_storage.FirebaseStorage.instance
          .ref('video/file$ts.mp4')
          .getDownloadURL();
      print("url $downloadURL");
      setState(() {
        fileAttach = downloadURL;
        progressShow = false;
        previewVideo = true;
        _videoPreview = downloadURL;
        previewImage = false;
      });

      _controller = VideoPlayerController.network(_videoPreview)
        ..initialize().then((_) {
          // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
          setState(() {});
        });
    } on firebase_core.FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  Future<WorkOrderModel> confirm(BuildContext context) async {
    String uid = await SharedPreferencesHelper.getUserID();
    var api = new ApiProvider();
    String imageUrl = "";
    String videoUrl = "";
    if (previewImage) {
      imageUrl = _imagePreview;
    }
    if (previewVideo) {
      videoUrl = _videoPreview;
    }
    print("_videoPreview $videoUrl");
    print("_imagePreview $imageUrl");
    var work = await api.createWorkOrder(
        uid,
        WorkRequestModel.technicainID,
        _noteTextController.text,
        imageUrl,
        videoUrl,
        GlobalLat,
        GlobalLong,
        WorkRequestModel.workType,
        context);
    return work;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'ข้อมูลเพิ่มเติม',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: ProgressHUD(
        child: Builder(
          builder: (context) => SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Container(
                      height: 140,
                      child: Card(
                          child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          children: [
                            Image(
                              height: 100,
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/car_call.png"),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 15, top: 8),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Text(
                                        "เรียกช่าง" +
                                            WorkTypeNameList[
                                                WorkRequestModel.workType],
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 15),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Text("โปรดระบุข้อมูลให้แก่ช่าง",
                                          style: TextStyle(
                                            fontWeight: FontWeight.normal,
                                            color: Colors.grey,
                                            fontSize: 18,
                                          )),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                    ),
                    Card(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, left: 10),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                child: Text(
                                  "ชื่อช่าง",
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                              child: ListTile(
                            title: Text(
                              WorkRequestModel.technicainName,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                            subtitle: RatingBar.builder(
                              itemSize: 18,
                              initialRating: 5,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            leading: CircleAvatar(
                                backgroundImage: NetworkImage(
                                    WorkRequestModel.technicainImage)),
                          )),
                        ],
                      ),
                    ),
                    Card(
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10, left: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                "ข้อความถึงผู้ให้บริการ",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: Container(
                            height: 150,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey,
                              ),
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            constraints: BoxConstraints(maxHeight: 200),
                            child: TextField(
                              textInputAction: TextInputAction.done,
                              controller: _noteTextController,
                              style: TextStyle(
                                fontSize: 16,
                              ),
                              keyboardType: TextInputType.multiline,
                              maxLines: 50,
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(10.0),
                                  border: null,
                                  hintText: "comment"),
                            ),
                          ),
                        ),
                      ]),
                    ),
                    Card(
                      child: Column(children: [
                        previewVideo
                            ? Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      height: 230,
                                      child: _controller.value.isInitialized
                                          ? AspectRatio(
                                              aspectRatio:
                                                  _controller.value.aspectRatio,
                                              child: VideoPlayer(_controller),
                                            )
                                          : SizedBox(),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 190, left: 15),
                                    child: Container(
                                      height: 35,
                                      width: 35,
                                      child: FloatingActionButton(
                                        onPressed: () {
                                          setState(() {
                                            _controller.value.isPlaying
                                                ? _controller.pause()
                                                : _controller.play();
                                          });
                                        },
                                        child: Icon(
                                          _controller.value.isPlaying
                                              ? Icons.pause
                                              : Icons.play_arrow,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox(),
                        previewImage
                            ? Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image(
                                  image: NetworkImage(_imagePreview),
                                ),
                              )
                            : SizedBox(),
                        progressShow
                            ? CircularProgressIndicator()
                            : SizedBox(
                                height: 0,
                              ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, left: 10),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Text(
                                "ภาพ/วิดีโอ (ไม่เกิน 10 วินาที) *ไม่บังคับ",
                                style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(11.0),
                          child: DottedBorder(
                            dashPattern: [12, 12],
                            strokeWidth: 2,
                            color: Colors.grey,
                            child: Container(
                              height: 150,
                              constraints: BoxConstraints(maxHeight: 200),
                              child: Center(
                                  child: InkWell(
                                onTap: () {
                                  showCupertinoModalPopup<void>(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        CupertinoActionSheet(
                                      actions: <CupertinoActionSheetAction>[
                                        CupertinoActionSheetAction(
                                          child: const Text('ถ่ายภาพ/image',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black,
                                                fontSize: 16,
                                              )),
                                          onPressed: () {
                                            Navigator.pop(context);
                                            _imgFromGallery();
                                          },
                                        ),
                                        CupertinoActionSheetAction(
                                          child: const Text('ถ่ายวิดีโอ/video',
                                              style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                color: Colors.black,
                                                fontSize: 16,
                                              )),
                                          onPressed: () {
                                            Navigator.pop(context);
                                            _videoFromGallery();
                                          },
                                        )
                                      ],
                                    ),
                                  );
                                },
                                child: Image(
                                  width: 100,
                                  image: AssetImage(
                                      "assets/images/add_file_image.png"),
                                ),
                              )),
                            ),
                          ),
                        ),
                      ]),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 1,
                      margin: EdgeInsets.all(10),
                      height: 56.0,
                      decoration: BoxDecoration(
                        color: HexColor.fromHex('#1E62AD'),
                        borderRadius: BorderRadius.circular(28),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 2,
                            offset: Offset(1, 1), // Shadow position
                          ),
                        ],
                      ),
                      child: TextButton(
                        onPressed: () {
                          if (GlobalLat == "" || GlobalLong == "") {
                            showAlertDialog(context, "");
                          } else {
                            confirm(context).then((value) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          WaitingTechnician(workOrder: value)));
                            });
                          }
                        },
                        child: Text(
                          "ถัดไป /  Next",
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
        requestPermisLocation().then((value) {});
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("ไม่สามารถใช้บริการได้"),
      content: Text("ต้องระบุ location ของคุณเพื่อแจ้งช่าง"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> requestPermisLocation() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled == false) {
      await openAppSettings();
    }
  }
}
