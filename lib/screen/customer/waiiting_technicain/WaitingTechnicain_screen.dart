import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/screen/customer/confirm_request/ConfirmRequest_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/tracking_technicain/TrackingTechnicain_screen.dart';

WorkOrderModel? WorkOrder;

class WaitingTechnician extends StatefulWidget {
  final WorkOrderModel workOrder;
  const WaitingTechnician({Key? key, required this.workOrder})
      : super(key: key);

  @override
  _WaitingTechnicianState createState() => _WaitingTechnicianState();
}

class _WaitingTechnicianState extends State<WaitingTechnician> {
  bool isStopped = false;
  void initState() {
    super.initState();

    Timer.periodic(new Duration(seconds: 3), (timer) {
      if (isStopped) {
        timer.cancel();
      }
      getStatus(timer);
    });
  }

  getStatus(timer) {
    var api = ApiProvider();
    api.getWorkingStatus(widget.workOrder.uid).then((value) {
      if (value.servicestatus == "reject") {
        timer.cancel();
        Navigator.pop(context);
      }

      if (value.servicestatus == "accept") {
        WorkOrder = value;
        timer.cancel();
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => TrackingTechnician(workid: value.uid)));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image(
                width: 150, image: AssetImage("assets/images/search_img.png")),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 50,
              height: 50,
              child: CircularProgressIndicator(
                valueColor:
                    AlwaysStoppedAnimation<Color>(HexColor.fromHex('#1E62AD')),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "กำลังแจ้งผู้ให้บริการ\nกรุณารอสักครู่...",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                color: Colors.black,
                fontSize: 18,
              ),
            ),
            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.white,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          margin: EdgeInsets.all(20),
          height: 56.0,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.red,
              width: 1, //                   <--- border width here
            ),
            color: Colors.white,
            borderRadius: BorderRadius.circular(28),
          ),
          child: TextButton(
            onPressed: () {
              setState(() {
                isStopped = true;
              });
              var api = ApiProvider();
              api.workReject(widget.workOrder.uid).then((value) {
                //Navigator.pop(context);
                if (value.providercommenturl == "") {
                  //case  ให้ระบบเลือกช่างให้
                  var nav = Navigator.of(context);
                  nav.pop();
                  nav.pop();
                } else {
                  var nav = Navigator.of(context);
                  nav.pop();
                  nav.pop();
                  nav.pop();
                  nav.pop();
                }
              });
            },
            child: Text(
              "ยกเลิก",
              style: TextStyle(color: Colors.red, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
