import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/choose_tectnicain/ChooseTechnicain_screen.dart';
import 'package:rengduan/screen/customer/confirm_request/ConfirmRequest_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';

import '../../../Constant.dart';

class RequestTechnicain extends StatefulWidget {
  const RequestTechnicain({Key? key}) : super(key: key);

  @override
  _RequestTechnicainState createState() => _RequestTechnicainState();
}

class _RequestTechnicainState extends State<RequestTechnicain> {
  final titles = ["เลือกช่างเอง", "ค้นหาช่างอัตโนมัติ"];
  final subtitles = [
    "สามารถเลือกและดูรายชื่อช่างด้วยตัวเอง",
    "ระบบจะเลือกช่างให้อัตโนมัติ"
  ];
  final icons = [Icons.chevron_right, Icons.chevron_right];
  String imageType = "";

  @override
  void initState() {
    super.initState();
    imageType = WorkTypeIconList[WorkRequestModel.imageType];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Row(
          children: [
            Container(
              child: Text(
                'เรียกช่าง ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              child: Text(
                'Service',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: HexColor.fromHex('#FAFAFB'),
      body: ProgressHUD(
        child: Builder(
          builder: (context) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    if (GlobalLat == "" || GlobalLong == "") {
                      showAlertDialog(context, "");
                    } else {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => ChooseTechnicain()));
                    }
                  },
                  child: Container(
                    height: 140,
                    child: Card(
                        child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Image(
                            height: 100,
                            fit: BoxFit.cover,
                            image: AssetImage(imageType),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 15, top: 8),
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      "เลือกช่างเอง",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 15),
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                        "สามารถเลือกและดูรายชื่อ\nช่างด้วยตัวเอง",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                          fontSize: 14,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Icon(icons[0]),
                        ],
                      ),
                    )),
                  ),
                ),
                InkWell(
                  onTap: () {
                    //print("xxxx");
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => ConfirmRequest()));
                  },
                  child: Container(
                    height: 140,
                    child: Card(
                        child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: [
                          Image(
                            height: 90,
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/search_img.png"),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 15, top: 8),
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: Text(
                                      "ค้นหาช่างอัตโนมัติ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 15),
                                  child: SizedBox(
                                    width: double.infinity,
                                    child: Text("ระบบจะเลือกช่างให้อัตโนมัติ",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey,
                                          fontSize: 14,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Icon(icons[1]),
                        ],
                      ),
                    )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
        requestPermisLocation().then((value) {});
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("ไม่สามารถใช้บริการได้"),
      content: Text("ต้องระบุ location ของคุณเพื่อแจ้งช่าง"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> requestPermisLocation() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled == false) {
      await openAppSettings();
    }
  }
}
