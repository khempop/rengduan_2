import 'package:flutter/material.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/select_work_type/select_worktype_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

class IntoUserApp extends StatefulWidget {
  const IntoUserApp({Key? key}) : super(key: key);

  @override
  _IntoUserAppState createState() => _IntoUserAppState();
}

class _IntoUserAppState extends State<IntoUserApp> {
  PageController? _pageController;
  int currentIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20, top: 20),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => SelectWorkType()),
                    (Route<dynamic> route) => false);
              },
              child: Text(
                "ข้าม",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 14,
                    fontWeight: FontWeight.w400),
              ),
            ),
          )
        ],
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          PageView(
            onPageChanged: (int page) {
              setState(() {
                currentIndex = page;
              });
            },
            controller: _pageController,
            children: <Widget>[
              pageViewer(
                content: Expanded(
                  child: WebView(
                    initialUrl: 'https://www.sopasss.com/userpage1',
                  ),
                ),
              ),
              pageViewer(
                content: Expanded(
                  child: WebView(
                    initialUrl: 'https://www.sopasss.com/userpage2',
                  ),
                ),
              ),
              pageViewer(
                content: Expanded(
                  child: WebView(
                    initialUrl: 'https://www.sopasss.com/userpage3',
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(bottom: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _buildIndicator(),
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          margin: EdgeInsets.all(20),
          height: 56.0,
          decoration: BoxDecoration(
            color: HexColor.fromHex('#1E62AD'),
            borderRadius: BorderRadius.circular(28),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2,
                offset: Offset(1, 1), // Shadow position
              ),
            ],
          ),
          child: TextButton(
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => SelectWorkType()),
                  (Route<dynamic> route) => false);
            },
            child: Text(
              "ไปต่อ /  Next",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }

  Widget _indicatior(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: 8,
      width: isActive ? 30 : 8,
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
          color: HexColor.fromHex('#1E62AD'),
          borderRadius: BorderRadius.circular(5)),
    );
  }

  List<Widget> _buildIndicator() {
    List<Widget> indicators = [];
    for (var i = 0; i < 3; i++) {
      if (currentIndex == i) {
        indicators.add(_indicatior(true));
      } else {
        indicators.add(_indicatior(false));
      }
    }
    return indicators;
  }

  Widget pageViewer({content}) {
    return Container(
      padding: EdgeInsets.only(left: 50, right: 50, bottom: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[content],
      ),
    );
  }
}
