import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/select_work_type/select_worktype_screen.dart';

class UserCompleteWork extends StatefulWidget {
  final workid;
  const UserCompleteWork({Key? key, required this.workid}) : super(key: key);

  @override
  _UserCompleteWorkState createState() => _UserCompleteWorkState();
}

class _UserCompleteWorkState extends State<UserCompleteWork> {
  var _comment = new TextEditingController();
  final _picker = ImagePicker();
  File? selectedFile;
  bool isSelectedFile = false;
  double _rating = 5;
  _imgFromGallery() async {
    print("xxx ");
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.gallery);
      print("xxx --- ");
      if (pickedFile != null) {
        setState(() {
          print("file path " + pickedFile.path);
          selectedFile = File(pickedFile.path);
          isSelectedFile = true;
        });
      }
    } catch (e) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'สำเร็จ!',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Container(
                  height: 140,
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        Image(
                          height: 100,
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/car_green.png"),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 15, top: 8),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Text(
                                    "สิ้นสุดการให้บริการ",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 18,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 15),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: Text(
                                      "แสดงความคิดเห็นและให้คะแนนแก่ผู้ให้บริการ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        color: Colors.grey,
                                        fontSize: 18,
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
                ),
                Card(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10, left: 10),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            child: Text(
                              "ให้คะแนนความพึงพอใจ",
                              style: TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.black,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: RatingBar.builder(
                            itemSize: 40,
                            initialRating: 5,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 5,
                            itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                              _rating = rating;
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          child: Text(
                            "แสดงความคิดเห็น",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(11.0),
                      child: Container(
                        height: 150,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        constraints: BoxConstraints(maxHeight: 200),
                        child: TextField(
                          controller: _comment,
                          textInputAction: TextInputAction.done,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          keyboardType: TextInputType.multiline,
                          maxLines: 50,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(10.0),
                              border: null,
                              hintText: ""),
                        ),
                      ),
                    ),
                  ]),
                ),
                /* Card(
                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          child: Text(
                            "ภาพ/วิดีโอ (ไม่เกิน 10 วินาที) *ไม่บังคับ",
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(11.0),
                      child: DottedBorder(
                        dashPattern: [12, 12],
                        strokeWidth: 2,
                        color: Colors.grey,
                        child: Container(
                          height: 150,
                          constraints: BoxConstraints(maxHeight: 200),
                          child: Center(
                              child: InkWell(
                            onTap: _imgFromGallery,
                            child: Image(
                              width: 100,
                              image: AssetImage(
                                  "assets/images/add_file_image.png"),
                            ),
                          )),
                        ),
                      ),
                    ),
                  ]),
                ),*/
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          margin: EdgeInsets.all(20),
          height: 56.0,
          decoration: BoxDecoration(
            color: HexColor.fromHex('#1E62AD'),
            borderRadius: BorderRadius.circular(28),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 2,
                offset: Offset(1, 1), // Shadow position
              ),
            ],
          ),
          child: TextButton(
            onPressed: () {
              var api = ApiProvider();
              api
                  .workUserComplete(
                      widget.workid, _comment.text, _rating.toString())
                  .then((value) {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => SelectWorkType()),
                    (Route<dynamic> route) => false);
              });
            },
            child: Text(
              "ตกลง / Submit",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
