import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/Service/APIProvider.dart';
import 'package:rengduan/screen/customer/confirm_request/ConfirmRequest_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';

class ChooseTechnicainDetail extends StatefulWidget {
  const ChooseTechnicainDetail({Key? key}) : super(key: key);

  @override
  _ChooseTechnicainDetailState createState() => _ChooseTechnicainDetailState();
}

class _ChooseTechnicainDetailState extends State<ChooseTechnicainDetail> {
  String _name = WorkRequestModel.technicainName;
  String _urlImage = WorkRequestModel.technicainImage;

  String _ratting = "0.0";
  String _typeName = WorkRequestModel.technicainTypeName;

  @override
  void initState() {
    super.initState();

    var api = ApiProvider();
    api.fetchUserInfoById(WorkRequestModel.technicainID).then((value) {
      setState(() {
        _ratting = value.rating ?? "0.0";
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'เลือกช่าง',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                height: 140,
                child: Card(
                    child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    children: [
                      CircleAvatar(
                          radius: 50.0,
                          backgroundImage: NetworkImage(_urlImage)),
                      Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 15, top: 8),
                              child: SizedBox(
                                width: double.infinity,
                                child: Text(
                                  _name,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: SizedBox(
                                width: double.infinity,
                                child: Text(_typeName,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey,
                                      fontSize: 18,
                                    )),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 60,
                                    child: Text(_ratting,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 25,
                                        )),
                                  ),
                                  RatingBar.builder(
                                    ignoreGestures: true,
                                    itemSize: 20,
                                    initialRating: double.parse(_ratting),
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding:
                                        EdgeInsets.symmetric(horizontal: 0.5),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        margin: EdgeInsets.all(20),
        height: 56.0,
        decoration: BoxDecoration(
          color: HexColor.fromHex('#1E62AD'),
          borderRadius: BorderRadius.circular(28),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2,
              offset: Offset(1, 1), // Shadow position
            ),
          ],
        ),
        child: TextButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (_) => ConfirmRequest()));
          },
          child: Text(
            "ถัดไป /  Next",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }
}
