import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:rengduan/Model/ProviderListModel.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/main.dart';
import 'package:rengduan/screen/customer/choose_tectnicain/ChooseTechnicainDetail_screen.dart';
import 'package:rengduan/screen/customer/confirm_request/ConfirmRequest_screen.dart';

class ChooseTechnicain extends StatefulWidget {
  const ChooseTechnicain({Key? key}) : super(key: key);

  @override
  _ChooseTechnicainState createState() => _ChooseTechnicainState();
}

class _ChooseTechnicainState extends State<ChooseTechnicain> {
  final icons = [Icons.ac_unit, Icons.access_alarm, Icons.access_time];

  List<ProviderListModel> listProvider = [];
  bool _progress = true;
  void initState() {
    super.initState();

    var api = ApiProvider();
    api
        .getListProvider(WorkRequestModel.workType, GlobalLat, GlobalLong)
        .then((value) {
      setState(() {
        listProvider = value;
        _progress = false;
      });

      value.forEach((element) {
        print(element.uid);
      });
    }).onError((error, stackTrace) {
      _progress = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'เลือกช่างเอง',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
      ),
      body: ProgressHUD(
        child: Builder(
          builder: (context) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: _progress
                ? Center(child: CircularProgressIndicator())
                : ListView.builder(
                    itemCount: listProvider.length,
                    itemBuilder: (context, index) {
                      return Card(
                          child: ListTile(
                              onTap: () {
                                WorkRequestModel.technicainID =
                                    listProvider[index].uid;
                                WorkRequestModel.technicainName =
                                    listProvider[index].name +
                                        " " +
                                        listProvider[index].surName;
                                WorkRequestModel.technicainImage =
                                    listProvider[index].imageUrl;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => ConfirmRequest()));
                              },
                              title: Text(
                                listProvider[index].name +
                                    " " +
                                    listProvider[index].surName,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              subtitle: RatingBar.builder(
                                ignoreGestures: true,
                                itemSize: 18,
                                initialRating:
                                    double.parse(listProvider[index].rating),
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 1.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              leading: CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      listProvider[index].imageUrl)),
                              trailing: Icon(Icons.chevron_right)));
                    }),
          ),
        ),
      ),
    );
  }
}
