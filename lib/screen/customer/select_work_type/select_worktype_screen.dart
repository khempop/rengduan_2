import 'package:flutter/material.dart';
import 'package:rengduan/Model/UserModel.dart';
import 'package:rengduan/Model/WorkRequestModel.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:rengduan/screen/customer/request_technician_type/RequestTechnicain_screen.dart';
import 'package:rengduan/screen/technician/edit_profile/EditProfile_screen.dart';

import '../../../Constant.dart';

class SelectWorkType extends StatefulWidget {
  const SelectWorkType({Key? key}) : super(key: key);

  @override
  _SelectWorkTypeState createState() => _SelectWorkTypeState();
}

class _SelectWorkTypeState extends State<SelectWorkType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Row(
          children: [
            Container(
              child: Text(
                'เรียกช่าง ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              child: Text(
                'Service',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => TechnicianEditProfile()));
              },
              child: CircleAvatar(
                  backgroundImage: NetworkImage(UserModel.imageUrl == ""
                      ? "https://firebasestorage.googleapis.com/v0/b/rengduan-2.appspot.com/o/avatar.png?alt=media&token=e4be6453-cfed-4c55-906b-661207025289"
                      : UserModel.imageUrl)),
            ),
          ),
        ],
      ),
      backgroundColor: HexColor.fromHex('#FAFAFB'),
      body: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 1,
        children: List.generate(8, (index) {
          return InkWell(
              onTap: () {
                //print("xxxx");
                WorkRequestModel.workType = WorkTypeIDList[index];
                WorkRequestModel.imageType = index;
                WorkRequestModel.technicainTypeName = WorkTypeList[index];
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => RequestTechnicain()));
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                margin: index % 2 == 0
                    ? EdgeInsets.only(left: 15, right: 0, top: 15)
                    : EdgeInsets.only(left: 15, right: 15, top: 15),
                padding: EdgeInsets.only(top: 10),
                // color: Colors.white,
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Image(
                          height: 100,
                          fit: BoxFit.cover,
                          image: AssetImage(WorkTypeIconList[index]),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          WorkTypeList[index],
                          style: TextStyle(
                              fontSize: 15.0, fontWeight: FontWeight.normal),
                        ),
                        Text(
                          WorkTypeEnList[index],
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.normal,
                              color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ),
              ));
        }),
      ),
    );
  }
}
