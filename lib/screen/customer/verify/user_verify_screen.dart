import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/screen/customer/into/into_screen.dart';
import 'package:rengduan/screen/customer/register/UserRegister_screen.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'package:path/path.dart' as Path;

class UserVerify extends StatefulWidget {
  const UserVerify({Key? key}) : super(key: key);

  @override
  _UserVerifyState createState() => _UserVerifyState();
}

class _UserVerifyState extends State<UserVerify> {
  final _picker = ImagePicker();
  File? selectedFile;
  bool isSelectedFile = false;
  ApiProvider service = ApiProvider();
  bool nextButtonState = false;

  firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;
  @override
  void initState() {
    super.initState();
  }

  _imgFromGallery() async {
    print("xxx --- ");
    try {
      final pickedFile = await _picker.getImage(source: ImageSource.camera);
      print("xxx --- ");
      if (pickedFile != null) {
        setState(() {
          print("file path " + pickedFile.path);
          selectedFile = File(pickedFile.path);
          isSelectedFile = true;
          uploadFile(pickedFile.path);
        });
      }
    } catch (e) {
      setState(() {});
    }
  }

  Future<void> uploadFile(String filePath) async {
    File file = File(filePath);

    String uid = await SharedPreferencesHelper.getUserID();
    String email = await SharedPreferencesHelper.getUserEmail();
    try {
      await firebase_storage.FirebaseStorage.instance
          .ref('user/verify/$uid.png')
          .putFile(file);

      String downloadURL = await firebase_storage.FirebaseStorage.instance
          .ref('user/verify/$uid.png')
          .getDownloadURL();
      print("url $downloadURL");
      service.updateProfileImage(email, downloadURL).then((value) {
        setState(() {
          nextButtonState = true;
        });
      });
    } on firebase_core.FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Column(
          children: [
            Container(
              child: Text(
                'ยืนยันตัวตนด้วยภาพถ่าย',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              child: Text(
                'PERSONAL INDENTIFICATION',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Center(
              child: Text(
                "ภาพบัตรต้องเห็นรายละเอียดชัดเจน โดยเฉพาะเลขที่บัตร รูปต้องเป็นเจ้าของบัตรถือบัตรประชาชนของตนเองเท่านั้น",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(
              height: 80,
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      DottedBorder(
                        dashPattern: [12, 12],
                        strokeWidth: 2,
                        color: Colors.grey,
                        child: Container(
                          decoration: BoxDecoration(image: showPicker(context)),
                          width: 150,
                          height: 150,
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: IconButton(
                              icon: Icon(
                                Icons.add_circle_rounded,
                                color: Colors.blue,
                              ),
                              onPressed: () {
                                _imgFromGallery();
                              },
                              iconSize: 50,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        "เพิ่มรูปภาพ",
                        style: TextStyle(fontSize: 15.0),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image:
                                AssetImage("assets/images/avatar_verify.png"),
                            fit: BoxFit.cover,
                          ),
                        ),
                        width: 158,
                        height: 156,
                        child: Text(
                          "",
                          style: TextStyle(fontSize: 10.0),
                        ),
                      ),
                      Text(
                        "รูปตัวอย่าง",
                        style: TextStyle(fontSize: 15.0),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width * 0.90,
        margin: EdgeInsets.all(20),
        height: 56.0,
        decoration: BoxDecoration(
          color: nextButtonState ? HexColor.fromHex('#1E62AD') : Colors.grey,
          borderRadius: BorderRadius.circular(28),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2,
              offset: Offset(1, 1), // Shadow position
            ),
          ],
        ),
        child: TextButton(
          onPressed: () {
            if (nextButtonState) {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => IntoUserApp()),
                  (Route<dynamic> route) => false);
            } else {}
          },
          child: Text(
            "ถัดไป /  Next",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      ),
    );
  }

  showPicker(BuildContext context) {
    if (isSelectedFile == false) {
      return DecorationImage(
        image: AssetImage("assets/images/white.jpg"),
        fit: BoxFit.cover,
      );
    } else {
      return DecorationImage(
        image: FileImage(selectedFile!),
        fit: BoxFit.cover,
      );
    }
  }
}
