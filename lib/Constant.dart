import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

var WorkTypeList = [
  "ช่างรถยนต์",
  "ช่างมอเตอร์ไซค์",
  "ช่างระบบไฟฟ้า",
  "ช่างประปา",
  "ช่างแอร์",
  "ช่างเครื่องใช้ไฟฟ้า",
  "ช่างกุญแจ",
  "รถยก"
];

Map WorkTypeNameList = {
  "01": "ช่างรถยนต์",
  "02": "ช่างมอเตอร์ไซค์",
  "03": "ช่างระบบไฟฟ้า",
  "04": "ช่างประปา",
  "05": "ช่างแอร์",
  "06": "ช่างเครื่องใช้ไฟฟ้า",
  "07": "ช่างกุญแจ",
  "08": "รถยก"
};

var WorkTypeIconList = [
  "assets/images/icon6.png",
  "assets/images/icon7.png",
  "assets/images/icon3.png",
  "assets/images/icon2.png",
  "assets/images/icon4.png",
  "assets/images/icon5.png",
  "assets/images/icon1.png",
  "assets/images/icon8.png"
];
var WorkTypeEnList = [
  "Auto mechanic",
  "Motorcycle",
  "Electric system",
  "Plumber",
  "Airconditioner",
  "Appliance",
  "Locksmith",
  "Forklift"
];
var WorkTypeIDList = ["01", "02", "03", "04", "05", "06", "07", "08"];

Map WorkTypeMapIDList = {
  "ช่างรถยนต์": "01",
  "ช่างมอเตอร์ไซค์": "02",
  "ช่างระบบไฟฟ้า": "03",
  "ช่างประปา": "04",
  "ช่างแอร์": "05",
  "ช่างเครื่องใช้ไฟฟ้า": "06",
  "ช่างกุญแจ": "07",
  "รถยก": "08"
};
String AvatarImage =
    "https://firebasestorage.googleapis.com/v0/b/rengduan-2.appspot.com/o/avatar.png?alt=media&token=e4be6453-cfed-4c55-906b-661207025289";

String term = '';

Future<String> loadAssetTerm() async {
  return await rootBundle.loadString('assets/term.txt');
}
