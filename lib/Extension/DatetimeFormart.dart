import 'package:intl/intl.dart';

class DatetimeFormat {
  static String formatDatetime(String datetimeStr) {
    return DateFormat("d MMM yyyy H:m").format(DateTime.parse(datetimeStr));
  }
}
