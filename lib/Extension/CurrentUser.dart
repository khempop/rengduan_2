import 'package:firebase_auth/firebase_auth.dart';

class CurrentUser {
  static String getUserID() {
    String userId = "";
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        userId = "";
      } else {
        userId = user.uid;
      }
    });

    return userId;
  }
}
