import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_progress_hud/flutter_progress_hud.dart';
import 'package:http/http.dart' show Client;
import 'package:rengduan/Model/MapPlacesModel.dart';
import 'package:rengduan/Model/PolyLineModel.dart';
import 'package:rengduan/Model/ProviderListModel.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Model/WorkOrderModel.dart';

class ApiProvider {
  String endpoint = "https://www.sopasss.com";
  static String MAP_API_KEY = "AIzaSyBFRWFU6NVJg9QaD10rDdfADo9DvpdBn50";
  Client client = Client();

  Future<UserInfomation> fetchUserInfo(String email) async {
    Map match = {"email": email};
    final response = await client.post(Uri.parse(endpoint + "/getuser/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    //print(response.body.toString());

    if (response.statusCode == 200) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<UserInfomation> fetchUserInfoById(String uid) async {
    Map match = {"uid": uid};
    final response = await client.post(Uri.parse(endpoint + "/getuserbyid/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    //print(response.body.toString());

    if (response.statusCode == 200) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<UserInfomation> saveProviderInfo(
      Map match, BuildContext context) async {
    final progress = ProgressHUD.of(context);
    progress!.show();
    final response = await client.post(
        Uri.parse(endpoint + "/create_provider/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());
    progress.dismiss();

    if (response.statusCode == 201) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      showAlertDialog(context, response.body.toString());
      return throw Exception('Failed to load ');
    }
  }

  Future<UserInfomation> updateProviderInfo(
      Map match, BuildContext context) async {
    final progress = ProgressHUD.of(context);
    progress!.show();
    //print(match);
    final response = await client.post(
        Uri.parse(endpoint + "/update_provider/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    //print(response.body.toString());
    progress.dismiss();

    if (response.statusCode == 200) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      showAlertDialog(context, response.body.toString());
      throw Exception('Failed to load ');
    }
  }

  Future<UserInfomation> saveUserInfo(Map match, BuildContext context) async {
    final progress = ProgressHUD.of(context);
    progress!.show();
    final response = await client.post(Uri.parse(endpoint + "/create_user/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());
    progress.dismiss();
    if (response.statusCode == 201) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      showAlertDialog(context, "กรุณากรอกข้อมูลให้ครบ");
      throw Exception('Failed to load ');
    }
  }

  Future<UserInfomation> updateProfileImage(String email, String url) async {
    Map match = {"email": email, "selfie_url": url};
    final response = await client.post(
        Uri.parse(endpoint + "/update_selfie_url/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      return UserInfomation.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> createWorkOrder(
      String userId,
      String providerId,
      String comment,
      String imageUrl,
      String videoUrl,
      String lat,
      String long,
      String servicetype,
      BuildContext context) async {
    Map map = {
      "requesttime": "",
      "accepttime": "",
      "finishtime": "",
      "credit": "300",
      "caseid": "",
      "userid": userId,
      "providerid": providerId,
      "comment": "",
      "message": comment,
      "providercomment": "",
      "commentimageurl": imageUrl,
      "commentvdourl": videoUrl,
      "messageimageurl": "",
      "messagevdourl": "",
      "providercommenturl": "",
      "providercommentvdourl": "",
      "servicetype": servicetype,
      "servicestatus": "init",
      "userstarrating": "",
      "providerstarrating": "",
      "userlat": lat,
      "userlong": long,
      "providerlat": "",
      "providerlong": ""
    };
    print(map);

    final progress = ProgressHUD.of(context);
    progress!.show();
    final response = await client.post(Uri.parse(endpoint + "/startevent/"),
        headers: {"Content-Type": "application/json"}, body: json.encode(map));
    print(response.body.toString());

    progress.dismiss();
    if (response.statusCode == 201) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load ');
    }
  }

  showAlertDialog(BuildContext context, String message) {
    Widget continueButton = TextButton(
      child: Text("ตกลง"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("เกิดข้อผิดพลาด"),
      content: Text(message),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<List<ProviderListModel>> getListProvider(
      String servicetype, String lat, String long) async {
    Map match = {"servicetype": servicetype, "userLat": lat, "userLng": long};
    print(match);
    final response = await client.post(Uri.parse(endpoint + "/getprovider/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson
          .map((m) => new ProviderListModel.fromJson(m))
          .toList();
    } else {
      return [];
      //return throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> getWorkingStatus(String uid) async {
    Map match = {"uid": uid};
    final response = await client.post(Uri.parse(endpoint + "/geteventdetail/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    // print(response.body.toString());

    if (response.statusCode == 200) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> workAccept(
      String uid, String providerid, String lat, String long) async {
    Map match = {
      "uid": uid,
      "servicestatus": "accept",
      "accepttime": "",
      "providerlat": lat,
      "providerlong": long,
      "providerid": providerid
    };
    print(match);
    if (providerid == "" || lat == "" || long == "") {
      return throw Exception('Failed to load ');
    }
    final response = await client.post(Uri.parse(endpoint + "/acceptevent/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> workReject(String uid) async {
    Map match = {
      "uid": uid,
      "servicestatus": "reject",
      "finishtime": "",
      "providerlat": "",
      "providerlong": ""
    };
    final response = await client.post(Uri.parse(endpoint + "/rejectevent/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> workComplete(
      String uid,
      String comment,
      String providercomment,
      String commentimageurl,
      String commentvdourl,
      String providercommenturl,
      String providercommentvdourl,
      String userstarrating,
      String providerstarrating) async {
    Map match = {
      "uid": uid,
      "servicestatus": "complete",
      "finishtime": "",
      "providercomment": providercomment,
      "providercommenturl": providercommenturl,
      "providercommentvdourl": providercommentvdourl,
      "providerstarrating": providerstarrating
    };
    final response = await client.post(Uri.parse(endpoint + "/completeevent/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<WorkOrderModel> workUserComplete(
    String uid,
    String comment,
    String userstarrating,
  ) async {
    Map match = {
      "uid": uid,
      "comment": comment,
      "commentimageurl": "",
      "commentvdourl": "",
      "userstarrating": userstarrating,
    };
    final response = await client.post(Uri.parse(endpoint + "/completeevent/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      return WorkOrderModel.fromJson(json.decode(response.body));
    } else {
      return throw Exception('Failed to load ');
    }
  }

  Future<List<WorkOrderModel>> getCurrnetWorking(String uid) async {
    Map match = {"uid": uid};
    print(match);
    final response = await client.post(Uri.parse(endpoint + "/getuserevent/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    print(response.body.toString());

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson.map((m) => new WorkOrderModel.fromJson(m)).toList();
    } else {
      return [];
    }
  }

  Future<List<WorkOrderModel>> getTechnicainWorkList(String uid) async {
    Map match = {"providerid": uid};

    final response = await client.post(Uri.parse(endpoint + "/geteventlist/"),
        headers: {"Content-Type": "application/json"},
        body: json.encode(match));
    // print(response.body.toString());

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson.map((m) => new WorkOrderModel.fromJson(m)).toList();
    } else {
      return [];
    }
  }

  Future<MapPlacesModel> getPlacesFromGoogle(String lat, String long) async {
    final response = await client.get(
      Uri.parse(
          "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&language=th&key=" +
              MAP_API_KEY),
      headers: {"Content-Type": "application/json"},
    );
    print(
        "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&language=th&key=" +
            MAP_API_KEY);
    print(response.body.toString());

    if (response.statusCode == 200) {
      return MapPlacesModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load ');
    }
  }

  Future<MapDurationModel> getDurationFromGoogle(
      String startlat, String startlong, String desLat, String desLong) async {
    final response = await client.get(
      Uri.parse(
          "https://maps.googleapis.com/maps/api/directions/json?origin=$startlat,$startlong&destination=$desLat,$desLong&key=" +
              MAP_API_KEY),
      headers: {"Content-Type": "application/json"},
    );
    //print(response.body.toString());

    if (response.statusCode == 200) {
      return MapDurationModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load ');
    }
  }

  Future<List<PolyLineModel>> getPolyLineFromGoogle(
      String startlat, String startlong, String desLat, String desLong) async {
    final response = await client.get(
      Uri.parse(
          "https://maps.googleapis.com/maps/api/directions/json?origin=$startlat,$startlong&destination=$desLat,$desLong&key=" +
              MAP_API_KEY),
      headers: {"Content-Type": "application/json"},
    );

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      return responseJson.map((m) => new PolyLineModel.fromJson(m)).toList();
    } else {
      return [];
    }
  }

  Future pointList() async {
    final response = await client.post(Uri.parse(endpoint + "/topuplist/"),
        headers: {"Content-Type": "application/json"});
    print(response.body.toString());

    if (response.statusCode == 200) {
      List responseJson = json.decode(response.body);
      print(responseJson);
      return responseJson;
    } else {
      return [];
    }
  }
}
