import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  static final String _kLanguageCode = "language";

  static Future<String> getLanguageCode() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_kLanguageCode) ?? 'en';
  }

  static Future<bool> setLanguageCode(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_kLanguageCode, value);
  }

  static Future<String> getUserID() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_id") ?? '';
  }

  static Future<bool> setUserID(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_id", value);
  }

  static Future<String> getUserEmail() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("user_email") ?? '';
  }

  static Future<bool> setUserEmail(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_email", value);
  }

  static Future<String> getUserName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("user_name") ?? '';
  }

  static Future<bool> setUserName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_name", value);
  }

  static Future<String> getUserTel() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("user_tel") ?? '';
  }

  static Future<bool> setUserTel(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user_tel", value);
  }
}
