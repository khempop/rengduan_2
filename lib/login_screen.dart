import 'dart:convert';
import 'dart:math';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rengduan/Model/UserInfomation.dart';
import 'package:rengduan/Service/ApiProvider.dart';
import 'package:rengduan/Service/SharedPreferencesHelper.dart';
import 'package:rengduan/screen/customer/select_work_type/select_worktype_screen.dart';
import 'package:rengduan/screen/select_user_type_screen.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:crypto/crypto.dart';
import 'package:rengduan/screen/technician/verify/TechnicainVerify_screen.dart';
import 'package:rengduan/screen/technician/worklist/TechnicianWorkList_screen.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'Model/UserModel.dart';
import 'Service/Repository.dart';
import 'package:rengduan/main.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  void initState() {
    super.initState();

    //SharedPreferencesHelper.getLanguageUserEmail()
    //   .then((value) => print(value));
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser!.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    FirebaseAuth.instance.authStateChanges().listen((User? user) async {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        print('User is signed in!');
        print(user.displayName);
        var api = ApiProvider();
        api.fetchUserInfo(user.email.toString()).then((userinfo) {
          //ลงทะเบียนแล้ว
          print("ลงทะเบียนแล้ว $userinfo.uid");
          UserModel.id = userinfo.uid;
          UserModel.profileName = userinfo.name + " " + userinfo.surname;
          UserModel.email = userinfo.email;
          UserModel.imageUrl = "";

          SharedPreferencesHelper.setUserID(userinfo.uid);
          SharedPreferencesHelper.setUserEmail(userinfo.email);
          SharedPreferencesHelper.setUserTel(userinfo.phone);

          if (userinfo.usertype == "user") {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => SelectWorkType()),
                (Route<dynamic> route) => false);
          } else {
            if (userinfo.userstatus == "inactive") {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => TechnicianVerify()),
                  (Route<dynamic> route) => false);
            } else {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => TechnicianWorkList()),
                  (Route<dynamic> route) => false);
            }
          }
        }).onError((error, stackTrace) {
          //ยังไม่ได้ลงทะเบียน
          print("ยังไม่ได้ลงทะเบียน");
          UserModel.id = user.uid;
          UserModel.profileName = user.displayName.toString();
          UserModel.email = user.email.toString();
          UserModel.imageUrl = user.photoURL.toString();

          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => SelectUserType()),
              (Route<dynamic> route) => false);
        });
      }
    });
    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

  String generateNonce([int length = 32]) {
    final charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<UserCredential> signInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    // Create an `OAuthCredential` from the credential returned by Apple.
    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        print('User is currently signed out!');
      } else {
        print(user.displayName);
        UserModel.id = user.uid;
        UserModel.profileName = user.displayName.toString();
        UserModel.email = user.email.toString();
        UserModel.imageUrl = user.photoURL.toString();
        UserModel.appleSign = true;

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => SelectUserType()),
            (Route<dynamic> route) => false);

        print('User is signed in!');
      }
    });

    // Sign in the user with Firebase. If the nonce we generated earlier does
    // not match the nonce in `appleCredential.identityToken`, sign in will fail.
    return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.2),
              child: Center(
                child: Container(
                    width: 300,
                    height: 250,
                    child: Image.asset('assets/images/logo.png')),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.90,
              margin: EdgeInsets.all(20),
              height: 56.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(28),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2,
                    offset: Offset(1, 1), // Shadow position
                  ),
                ],
              ),
              child: TextButton(
                  onPressed: () {},
                  child: TextButton.icon(
                    onPressed: () {
                      signInWithGoogle();
                      //  Navigator.push(context,
                      //     MaterialPageRoute(builder: (_) => SelectUserType()));
                    },
                    icon: new Image.asset("assets/images/google_icon.png"),
                    label: Text(
                      "Sign in with Google",
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  )),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.90,
              margin: EdgeInsets.only(left: 20, right: 20, top: 0),
              height: 56.0,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(28),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2,
                    offset: Offset(1, 1), // Shadow position
                  ),
                ],
              ),
              child: TextButton(
                  onPressed: () {},
                  child: TextButton.icon(
                    onPressed: () {
                      signInWithApple();
                      //  Navigator.push(context,
                      //     MaterialPageRoute(builder: (_) => SelectUserType()));
                    },
                    icon: new Image.asset("assets/images/apple-256.png"),
                    label: Text(
                      "Sign in with Apple",
                      style: TextStyle(color: Colors.white, fontSize: 19),
                    ),
                  )),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        child: TextButton(
          onPressed: () {},
          child: Text(
            'By creating new account. \n I accept the TERMS OF THE SERVICE.',
            style: TextStyle(color: Colors.black, fontSize: 14),
          ),
        ),
        elevation: 0,
      ),
    );
  }
}
