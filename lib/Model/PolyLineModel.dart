class PolyLineModel {
  double lat;
  double long;
  PolyLineModel({
    required this.lat,
    required this.long,
  });

  factory PolyLineModel.fromJson(Map<String, dynamic> json) {
    return PolyLineModel(
      lat: json['routes'][0]["legs"][0]["step"]["start_location"]["lat"],
      long: json['routes'][0]["legs"][0]["step"]["start_location"]["long"],
    );
  }
}
