class UserInfomation {
  String uid;
  String name;
  String surname;
  String email;
  String phone;
  String selfieUrl;
  String createtime;
  String servicetype;
  String usertype;
  String userstatus;
  String credit;
  String agreement;
  String profileImage;
  String? rating;

  UserInfomation(
      {required this.uid,
      required this.name,
      required this.surname,
      required this.email,
      required this.phone,
      required this.selfieUrl,
      required this.createtime,
      required this.servicetype,
      required this.usertype,
      required this.userstatus,
      required this.credit,
      required this.agreement,
      required this.profileImage,
      required this.rating});

  factory UserInfomation.fromJson(Map<String, dynamic> json) {
    return UserInfomation(
        uid: json['uid'],
        name: json['name'],
        surname: json['surname'],
        email: json['email'],
        phone: json['phone'],
        selfieUrl: json['selfie_url'],
        createtime: json['createtime'],
        servicetype: json['servicetype'],
        usertype: json['usertype'],
        userstatus: json['userstatus'],
        credit: json['credit'],
        agreement: json['agreement'],
        profileImage: json['profile_image'],
        rating: json["rating"] ?? "0.0");
  }
}
