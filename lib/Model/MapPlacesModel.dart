class MapPlacesModel {
  String addressName;

  MapPlacesModel({
    required this.addressName,
  });

  factory MapPlacesModel.fromJson(Map<String, dynamic> json) {
    return MapPlacesModel(
      addressName: json['results'][0]["formatted_address"],
    );
  }
}

class MapDurationModel {
  String duration;

  MapDurationModel({
    required this.duration,
  });

  factory MapDurationModel.fromJson(Map<String, dynamic> json) {
    return MapDurationModel(
      duration: json['routes'][0]["legs"][0]["duration"]["text"],
    );
  }
}
