class ProviderListModel {
  String uid;
  String name;
  String surName;
  String imageUrl;
  String servicetype;
  String rating;
  String lat;
  String long;

  ProviderListModel(
      {required this.uid,
      required this.name,
      required this.surName,
      required this.imageUrl,
      required this.servicetype,
      required this.rating,
      required this.lat,
      required this.long});

  factory ProviderListModel.fromJson(Map<String, dynamic> json) {
    return ProviderListModel(
        uid: json['uid'],
        name: json['name'],
        surName: json['surname'],
        imageUrl: json['selfie_url'],
        servicetype: json['servicetype'],
        rating: '5.0',
        lat: '',
        long: '');
  }
}
